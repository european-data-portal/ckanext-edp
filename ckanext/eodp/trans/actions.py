"""
Provides translation specific actions
"""
__author__ = 'fki'

import ckan.plugins.toolkit as tk
from ckan.logic.action.patch import package_patch
from ckanext.eodp import translation as trans_service
from ckanext.eodp.trans import auto_translation
from ckanext.eodp.trans import *

import datetime
import logging
log = logging.getLogger(__name__)


def package_translate(context, data_dict):
    """
    Handles the callback from the translation service
    :type data_dict: dict
    :param context:
    :param data_dict:
    :return:
    """
    if TRANSFIELD in data_dict:
        translation = data_dict[TRANSFIELD]
        old_dict = tk.get_action('package_show')(context, {'id': data_dict['id']})
        meta = old_dict[METAFIELD]
        now = datetime.datetime.now().isoformat()

        if 'originalLanguage' in data_dict:
            base_language = data_dict['originalLanguage']
        else:
            base_language = auto_translation.get_base_language(old_dict)

        languages = set(meta['languages'])

        package_translation = {}
        for field in translation:
            package_translation[field] = {}
            languages.add(field)
            meta_update = {
                'tag': '%s-t-%s-t0-mtec' % (field, base_language),
                'received': now
            }
            if field in meta and isinstance(meta[field], dict):
                meta[field].update(meta_update)
            else:
                meta[field] = meta_update

            for sp_field in FIELDS:
                package_translation[field][sp_field] = translation[field][sp_field]

        meta['status'] = 'finished'
        meta_default = meta['default']

        # If the default language in dict, put it to the base fields
        if DEFAULT in package_translation:
            if meta_default != DEFAULT:
                # Put the former base language to translation
                package_translation[base_language] = {}
                for field in FIELDS:
                    if field in package_translation[DEFAULT]:
                        package_translation[base_language][field] = old_dict[field]
                        data_dict[field] = package_translation[DEFAULT][field]
                del package_translation[DEFAULT]
                meta['default'] = DEFAULT
            else:
                # Just update the default fields
                for field in FIELDS:
                    if field in package_translation[DEFAULT]:
                        data_dict[field] = package_translation[DEFAULT][field]
                del package_translation[DEFAULT]

        meta['languages'] = list(languages)
        data_dict[METAFIELD] = meta
        # Get already existing translations and merge them
        if TRANSFIELD in old_dict:
            old_dict[TRANSFIELD].update(package_translation)
            data_dict[TRANSFIELD] = old_dict[TRANSFIELD]
        else:
            data_dict[TRANSFIELD] = package_translation

        # Handle the resource translations
        data_dict_resources = []
        for resource in old_dict['resources']:
            resource_meta_default = meta_default
            resource_translation = {}
            resource_id = resource['id']
            name_key = resource_id + '_' + 'name'
            des_key = resource_id + '_' + 'description'
            for field in translation:
                if name_key or des_key in field:
                    resource_translation[field] = {}
                    if name_key in translation[field]:
                        resource_translation[field]['name'] = translation[field][name_key]
                    if des_key in translation[field]:
                        resource_translation[field]['description'] = translation[field][des_key]
                    if not resource_translation[field]:
                        del resource_translation[field]

            # If the default language in dict, put it to the base fields
            if TRANSFIELD not in resource:
                resource_meta_default = base_language

            if DEFAULT in resource_translation:
                res_fields = ['name', 'description']
                if resource_meta_default != DEFAULT:
                    # Put the former base language to translation
                    resource_translation[base_language] = {}
                    for field in res_fields:
                        if field in resource_translation[DEFAULT]:
                            resource_translation[base_language][field] = resource[field]
                            resource[field] = resource_translation[DEFAULT][field]
                    del resource_translation[DEFAULT]
                else:
                    for field in res_fields:
                        if field in resource_translation[DEFAULT]:
                            resource[field] = resource_translation[DEFAULT][field]
                    del resource_translation[DEFAULT]

            if resource_translation:
                if TRANSFIELD in resource:
                    resource[TRANSFIELD].update(resource_translation)
                else:
                    resource[TRANSFIELD] = resource_translation

            data_dict_resources.append(resource)

        data_dict['resources'] = data_dict_resources
        data_dict['state'] = 'active'
        data_dict['translated'] = True
        result = tk.get_action('package_patch')(context, data_dict)
        return result
    else:
        return None
