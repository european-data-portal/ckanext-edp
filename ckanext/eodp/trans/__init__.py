__author__ = 'fki'

from ckanext.eodp.config import CONFIG
import ckan.plugins.toolkit as tk

config_key = 'translation'

AUTO_TRANS = CONFIG.getboolean(config_key, 'auto_translation')
LANGUAGES = CONFIG.getlist(config_key, 'languages')
FIELDS = CONFIG.getlist(config_key, 'fields')
DEFAULT = CONFIG.get(config_key, 'default_language')
TRANSFIELD = CONFIG.getoptional(config_key, 'translation_field_name', 'translation')
METAFIELD = TRANSFIELD + '_meta'
TRANS_SERVICE_URL = CONFIG.get(config_key, 'service')
CALLBACK_URL = CONFIG.getoptional(config_key, 'callback_base', None)
API_KEY = CONFIG.get(config_key, 'api_key')
MOCK_SERVICE = CONFIG.getboolean(config_key, 'mock_service')
SUPPORTED_LANGUAGES = [DEFAULT] + LANGUAGES


Invalid = tk.Invalid
convert_package_name_or_id_to_id = tk.get_converter('convert_package_name_or_id_to_id')