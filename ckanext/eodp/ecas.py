from caslib import *
import logging
import re
from repoze.who.api import get_api
from pylons.controllers.util import redirect

import ckan.lib.helpers as h
import ckan.plugins.toolkit as tk

from config import CONFIG

log = logging.getLogger(__name__)

user_show = tk.get_action('user_show')
NotFound = tk.ObjectNotFound

SERVER = CONFIG.get('ecas', 'server')
ADMIN_USER = CONFIG.get('ecas', 'admin_user')
PASSWORD = CONFIG.get('ecas', 'password')
CALLBACK_URL = h.url_for(controller='ckanext.eodp.ecas:EcasController', action='callback',
                         qualified=True)


class EcasController(tk.BaseController):

    def login(self):
        url_redirect = tk.request.params.get('url_redirect')
        callback_url = h.url_for(controller='ckanext.eodp.ecas:EcasController', action='callback', qualified=True)
        redirect_url = SERVER + "/login?service=" + callback_url

        if url_redirect:
            redirect(redirect_url + "?url_redirect=" + url_redirect)

        return redirect(redirect_url)

    def callback(self):
        url_redirect = tk.request.params.get('url_redirect')
        callback_url = h.url_for(controller='ckanext.eodp.ecas:EcasController', action='callback', qualified=True)
        new_callback_url = callback_url

        if url_redirect:
            new_callback_url += "?url_redirect=" + url_redirect

        cas_client = CASClient(SERVER, new_callback_url, auth_prefix="")
        ticket_from_cas = tk.request.GET['ticket']
        cas_response = cas_client.cas_serviceValidate(ticket_from_cas)
        (truth, user) = (cas_response.success, cas_response.user)
        try:
            user = user.split("#")[1]
        except IndexError:
            pass
        # for attr in dir(cas_response):
        #     log.error ("obj.%s = %s" % (attr, getattr(cas_response, attr)))
        if truth:
            if self._user_exists(user):
                self._login_ecas_user(user, url_redirect)
            else:
                self._create_user(user, 'dummy@europeandataportal.eu')
                self._login_ecas_user(user, url_redirect)
        else:
            h.flash_error('Could not log in with ECAS')
            return tk.redirect_to('/')

    def _create_user(self, username, email):
        user_create = tk.get_action('user_create')
        user = user_show({}, {
            'id': ADMIN_USER
        })
        user_create({
            'user': user['name']
        }, {
            'name': username,
            'password': PASSWORD,
            'email': email
        })

    def _user_exists(self, name):
        try:
            user = user_show({}, {
                'id': name
            })
            return user
        except NotFound:
            return False

    def _login_ecas_user(self, username, url_redirect):
        who_api = get_api(tk.request.environ)
        response = tk.response

        creds = {}
        creds['login'] = username
        creds['password'] = PASSWORD

        authenticated, headers = who_api.login(creds)
        if authenticated:
            response.headers = headers + response.headerlist
            if url_redirect:
                if re.match(r'/data/[a-z]{2}//*', url_redirect):
                    url_redirect = url_redirect[8:]
                elif str(url_redirect).startswith('/data/'):
                    url_redirect = url_redirect[len('/data'):]
                tk.redirect_to(str(url_redirect))
            else:
                tk.redirect_to('user_dashboard')
        else:
            h.flash_error('Could not log in with ECAS')
            tk.redirect_to('/')

