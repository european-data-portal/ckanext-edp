# -*- coding: utf-8 -*-

from rdflib import Graph
from rdflib.exceptions import Error
from SPARQLWrapper.SPARQLExceptions import EndPointInternalError, QueryBadFormed

from ckanext.eodp.sparql_store.graph import paneodp

import ckanext.eodp.sparql_store.dcatap as dcatap
from ckanext.eodp.sparql_store.dcatap import DCAT, DATASET, DISTRIBUTION, CATALOG

import logging
log = logging.getLogger(__name__)

from ckanext.eodp.helpers import get_field


__author__ = 'sim'


def create_organization(organization):
    """

    :param organization:
    :type organization: dict
    """
    global paneodp
    org = CATALOG[organization['name']]

    ng = paneodp.get_context(org)

    if len(ng) > 0:
        log.error("organization %s already exists" % org)
        return

    # every organization will be wrapped by a catalog
    dcatap.catalog(ng, organization)


def update_organization(organization):
    """

    :param organization:
    :type organization: dict
    """
    global paneodp
    ng = paneodp.get_context(CATALOG[organization['name']])
    dcatap.catalog(ng, organization)


def delete_organization(organization_name):
    """

    :param organization_name:
    :type organization_name: string
    """
    global paneodp
    org = CATALOG[organization_name]
    ng = paneodp.get_context(org)
    for d in ng.objects(org, DCAT.dataset):
        paneodp.remove_context(paneodp.get_context(d))

    paneodp.remove_context(ng)


def create_dataset(package):
    """

    :param package:
    :type package: dict
    """
    global paneodp

    g = Graph()

    dataset = dcatap.dataset(g, package)
    record = dcatap.catalog_record(g, dataset, package)

    try:
        # find correct catalog from associated organisation
        # if not found, put dataset into the common catalog
        catalog = CATALOG.Common
        if 'organization' in package:
            organization = package['organization']
            if organization:
                catalog = CATALOG[organization['name']]

        cg = paneodp.get_context(catalog)
        if len(cg) == 0:
            cg = paneodp.get_context(CATALOG.Common)

        cg.add( (catalog, DCAT.record, record) )
        cg.add( (catalog, DCAT.dataset, dataset) )

        ng = paneodp.get_context(dataset)
        ng += g
    except (Error, QueryBadFormed, EndPointInternalError) as e:
        log.error("sparqlsync (%s) creating dataset failed: %s" % (package['name'], e.message.decode('utf-8')))
        return False

    resources = get_field(package, 'resources', [])
    for resource in resources:
        g = Graph()
        distribution = DISTRIBUTION[resource['id']]
        try:
            g.add( (dataset, DCAT.distribution, distribution) )
            dcatap.distribution(g, package, resource)
            ng += g
        except (Error, QueryBadFormed, EndPointInternalError) as e:
            log.error("sparqlsync (%s) creating resource failed: %s" % (resource['id'], e.message.decode('utf-8')))

    paneodp.touch_catalog(catalog)
    return True


def update_dataset(package):
    """

    :param package: the complete updated ckan package
    :type package: dict
    """

    # We first follow the same strategy like ckan: Delete and create.

    delete_dataset(package['name'], package['organization']['name'])
    return create_dataset(package)


def delete_dataset(package_name, organization_name):
    """
    Delete a dataset and all belonging distributions
    :param package_name: The name of the given dataset which is part of the URI ref
    :type package_name: string
    :param organization_name: The name of the organization
    :type package_name: string
    """
    global paneodp

    dataset = DATASET[package_name]

    paneodp.remove_context(paneodp.get_context(dataset))
    cng = paneodp.get_context(CATALOG[organization_name])
    cng.remove( (None, None, dataset) )

    paneodp.touch_catalog(CATALOG[organization_name])


def create_distribution(resource, package):
    """
    Create a distribution for a dataset
    :param resource: The ckan resource representing the distribution
    :type resource: dict
    :param package: The package representing the corresponding dataset
    :type package: Package
    """
    global paneodp
    if not isinstance(package, dict):
        p = package.as_dict()
    else:
        p = package
    ng = paneodp.get_context(DATASET[p['name']])
    distribution = dcatap.distribution(ng, p, resource)
    ng.add( (DATASET[p['name']], DCAT.distribution, distribution) )


def update_distribution(resource, package):
    """

    :param resource:
    :type resource: dict
    :param package:
    :type package: Package
    """
    delete_distribution(resource['id'])
    create_distribution(resource, package)


def delete_distribution(resource_id):
    """

    :param resource_id:
    :type resource_id: string
    """
    global paneodp
    distribution = DISTRIBUTION[resource_id]
    dataset = paneodp.subject(DCAT.distribution, distribution)
    ng = paneodp.get_context(dataset)
    ng.remove( (distribution, None, None) )
    ng.remove( (None, None, distribution) )


def connected():
    """
    Tell if the sparql store is connected (e.g. correctly configured)
    :return: True if connected, False otherwise
    :rtype: bool
    """
    return paneodp.connected
