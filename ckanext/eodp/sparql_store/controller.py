import ckan.plugins.toolkit as tk
import requests
import logging
from ckanext.eodp.config import CONFIG

log = logging.getLogger(__name__)

redirect = tk.redirect_to


class ContentTypeController(tk.BaseController):

    def negotiate(self, id, format):
        if format == 'rdf':
            accept_header = 'application/rdf+xml'
            content_type = 'application/rdf+xml;charset=utf-8'
        elif format == 'n3':
            accept_header = 'text/rdf+n3'
            content_type = 'text/rdf+n3;charset=utf-8'
        else:
            redirect(controller='package', action='read', id=id)
            return

        headers = {
            'Accept': accept_header
        }
        log.info(accept_header)
        name = id
        host = CONFIG.get('sparql_store', 'host')
        url = host + '/set/data/' + name
        try:
            result = requests.get(
                url,
                headers=headers,
                timeout=5
            )
            if result.status_code == 200:
                tk.response.headers['Content-Type'] = content_type
                return result.text
            else:
                raise Exception(('SPARQL-Endpoint %s returned %s Status Code') % (url, str(result.status_code)))

        except (requests.Timeout, requests.ConnectionError) as e:
                log.error(e.message)
                raise Exception(e.message)

