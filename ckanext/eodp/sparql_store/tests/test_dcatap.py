__author__ = 'sim'

import unittest

from rdflib import Literal, URIRef, Graph, BNode
from rdflib.namespace import Namespace, FOAF, DCTERMS, RDF, XSD, OWL

from ckanext.eodp.sparql_store.dcatap import _object_value, _agent

import logging
log = logging.getLogger(__name__)


class DcatapTestCase(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_object_value_with_literal_and_params(self):
        obj_value = _object_value(Literal, "test object", {'datatype': XSD.dateTime})

        self.assertIsInstance(obj_value, Literal)
        self.assertEquals(obj_value.datatype, XSD.dateTime)
        self.assertIsNone(obj_value.language)

    def test_object_value_with_literal_without_params(self):
        obj_value = _object_value(Literal, "test object", {})

        self.assertIsInstance(obj_value, Literal)
        self.assertIsNone(obj_value.datatype)
        self.assertIsNone(obj_value.language)

    def test_object_value_with_namespace(self):
        obj_value = _object_value(Namespace("urn:test/"), "test object", {})

        self.assertIsInstance(obj_value, URIRef)
        self.assertEquals(obj_value, URIRef("urn:test/test object"))

    def test_object_value_with_uriref(self):
        obj_value = _object_value(URIRef, "urn:test/test object", {})

        self.assertIsInstance(obj_value, URIRef)
        self.assertEquals(obj_value, URIRef("urn:test/test object"))

    def test_agent(self):
        graph = Graph()
        subject = URIRef("urn:subject")
        _agent(graph, subject, FOAF.agent, URIRef, {'name': "Test Agent", 'email': "mailto:testagent@test.com"}, {})

        self.assertIn((URIRef("urn:subject"), FOAF.agent, None), graph)
        self.assertIn((None, FOAF.name, Literal("Test Agent")), graph)
        self.assertIn((None, FOAF.mbox, URIRef("mailto:testagent@test.com")), graph)


if __name__ == '__main__':
    unittest.main()
