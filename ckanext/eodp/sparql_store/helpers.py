import ckanext.eodp.sparql_store.vocabularies as voc
from ckanext.eodp.helpers import language
import ckan.plugins.toolkit as tk


def resolve_resource(value, field):
    result = None
    if not voc.is_available():
        return result

    context = {
        'request': tk.request
    }
    lang = language(context)

    if field in ['accrual_periodicity']:
        result = voc.frequency_label(value, lang=lang)

    if field in ['language']:
        result = voc.language_label(value, lang=lang)

    if field in ['publisher']:
        result = voc.corporatebody_label(value, lang=lang)

    return result
