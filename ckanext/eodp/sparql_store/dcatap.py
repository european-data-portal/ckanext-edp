# -*- coding: utf-8 -*-

from rdflib import Literal, URIRef, Graph, BNode
from rdflib.namespace import Namespace, FOAF, DCTERMS, RDF, XSD, OWL, RDFS

import urllib2

import dateutil.parser
import validators

from ckanext.eodp.helpers import get_field, get_extra_field

import logging
log = logging.getLogger(__name__)

__author__ = 'sim'

ADMS = Namespace("http://www.w3.org/ns/adms#")
DCAT = Namespace("http://www.w3.org/ns/dcat#")
V = Namespace("http://www.w3.org/2006/vcard/ns#")
SPDX = Namespace("http://spdx.org/rdf/terms#")
SCHEMA = Namespace("http://schema.org/")
LOCN = Namespace("http://www.w3.org/ns/locn#")
ORG = Namespace("http://www.w3.org/ns/org#")

PANEDP = Namespace("http://europeandataportal.eu/")
THEME = Namespace("http://publications.europa.eu/resource/authority/data-theme")
CATEGORY = Namespace(THEME['/'])

FILETYPE = Namespace("http://publications.europa.eu/resource/authority/file-type/")
CORPORATE_BODY = Namespace("http://publications.europa.eu/resource/authority/corporate-body/")
LANGUAGE = Namespace("http://publications.europa.eu/resource/authority/language/")
PUBLISHERTYPE = Namespace("http://purl.org/adms/publishertype/")
CHANGETYPE = Namespace("http://purl.org/adms/changetype/")
CATALOG = Namespace(PANEDP["id/catalog/"])
DATASET = Namespace(PANEDP["set/data/"])
RECORD = Namespace(PANEDP["set/record/"])
DISTRIBUTION = Namespace(PANEDP["set/distribution/"])

DEFAULT_LANG = 'en'

_authority_themes = {
    'agriculture-fisheries-forestry-and-food': "AGRI",
    'education-culture-and-sport': "EDUC",
    'environment': "ENVI",
    'energy': "ENER",
    'transport': "TRAN",
    'science-and-technology': "TECH",
    'economy-and-finance': "ECON",
    'population-and-society': "SOCI",
    'health': "HEAL",
    'government-and-public-sector': "GOVE",
    'regions-and-cities': "REGI",
    'justice-legal-system-and-public-safety': "JUST",
    'international-issues': "INTR"
}

_authority_filetypes = {
    'TAR': "",
    'GZIP': "",
    'ZIP': "",
    'AZW': "",
    'EPUB': "",
    'MOBI': "",
    'GIF': "",
    'JPEG': "",
    'TIFF': "",
    'PNG': "",
    'EPS': "",
    'CSS': "",
    'PDF': "",
    'PDFA1A': "",
    'PDFA1B': "",
    'PDFX': "",
    'METS': "",
    'METS_ZIP': "",
    'PPSX': "",
    'PPS': "",
    'PPT': "",
    'PPTX': "",
    'XLS': "",
    'XLSX': "",
    'XSLFO': "",
    'XSLT': "",
    'DTD_SGML': "",
    'DTD_XML': "",
    'SCHEMA_XML': "",
    'FMX2': "",
    'FMX3': "",
    'FMX4': "",
    'RDF_XML': "",
    'RDF_TURTLE': "",
    'SGML': "",
    'SKOS_XML': "",
    'OWL': "",
    'XML': "",
    'SPARQLQ': "",
    'SPARQLQRES': "",
    'DOC': "",
    'DOCX': "",
    'ODT': "",
    'TXT': "",
    'RTF': "",
    'HTML': "",
    'XHTML': "",
    'CSV': "",
    'MDB': "",
    'DBF': "",
    'MOP': "",
    'E00': "",
    'MXD': "",
    'KML': "",
    'TSV': "",
    'JSON': "",
    'KMZ': "",
    'GML': "",
    'RSS': "",
    'ODS': "",
    'INDD': "",
    'PSD': "",
    'PS': "",
    'ODF': "",
    'TAR_XZ': "",
    'TAR_GZ': "",
    'RDF': "",
    'XLIFF': "",
    'OVF': "",
    'JSON_LD': "",
    'RDF_N_TRIPLES': "",
    'HDF': "",
    'NETCDF': "",
    'SDMX': "",
    'JPEG2000': "",
    'SHP': "",
    'GDB': "",
    'GMZ': "",
    'ECW': "",
    'GRID_ASCII': "",
    'DMP': "",
    'LAS': "",
    'LAZ': "",
    'TAB': "",
    'TAB_RSTR': "",
    'WORLD': "",
    'TMX': "",
    'ATOM': "",
    'OCTET': "",
    'BIN': "",
    'ODC': "",
    'ODB': "",
    'ODG': "",
    'BMP': "",
    'DCR': "",
    'XYZ': "",
    'MAP_PRVW': "",
    'MAP_SRVC': "",
    'REST': "",
    'MSG_HTTP': "",
    'OP_DATPRO': ""
}


def _object_value(obj_type, obj, params_or_converter):
    if obj_type.__class__ == Namespace:
        obj_value = obj_type.term(obj)

    if obj_type == URIRef:
        if not isinstance(params_or_converter, dict):
            conv = params_or_converter(obj)
            if conv:
                obj_value = obj_type(conv)
            else:
                return None
        else:
            obj_value = obj_type(obj)

    if obj_type == Literal:
        if 'datatype' in params_or_converter.keys():
            obj_value = obj_type(obj, datatype=params_or_converter['datatype'])
        elif 'lang' in params_or_converter.keys():
            obj_value = obj_type(obj, lang=params_or_converter['lang'])
        else:
            obj_value = obj_type(obj)

    if isinstance(obj_value, basestring):
        return obj_type(obj_value.strip())
    else:
        return obj_value


def _urlencode(url):
    if isinstance(url, basestring):
        stripped = url.strip()
    else:
        stripped = url

    if not validators.url(stripped):
        return None

    try:
        scheme = stripped.split(":")[0]
        rest = stripped[len(scheme)+1:]
        return scheme + ":" + urllib2.quote(urllib2.unquote(rest.encode('utf-8')))
    except Exception as e:
        log.warning("[_urlencode] encoding url failed: " + url + " (" + e.message + ")")
        return None


def _validate_email(address):
    tovalidate = address.strip().split()[0]
    if not tovalidate.startswith('mailto:'):
        tovalidate = 'mailto:' + tovalidate

    if validators.email(tovalidate[7:]):
        return tovalidate
    else:
        return None


def _many(graph, subject, predicate, obj_type, obj, params):
    value = _object_value(obj_type, obj, params)
    if value:
        try:
            graph.add( (subject, predicate, value) )
        except Exception as e:
            log.exception("[_many] many %s for %s of %s: %s", obj, predicate, subject, e.message)


def _one(graph, subject, predicate, obj_type, obj, params):
    value = _object_value(obj_type, obj, params)
    if value:
        try:
            graph.set( (subject, predicate, value) )
        except Exception as e:
            log.warning("[_one] one %s for %s of %s: %s", obj, predicate, subject, e.message)


def _add_one(graph, subject, predicate, obj_type, obj, params):
    value = _object_value(obj_type, obj, params)
    if value:
        try:
            graph.add( (subject, predicate, value) )
        except Exception as e:
            log.warning("[_add_one] adding one %s for %s of %s: %s", obj, predicate, subject, e.message)


def _multilingual(graph, subject, predicate, obj_type, obj, params):
    params.update({'lang': DEFAULT_LANG})
    value = _object_value(obj_type, obj, params)
    if value:
        try:
            graph.add( (subject, predicate, value) )
        except Exception as e:
            log.warning("[_multilingual] multilingual %s for %s of %s: %s", obj, predicate, subject, e.message)


def _keyword(graph, subject, predicate, obj_type, obj, params):
    _many(graph, subject, predicate, obj_type, obj['display_name'], params)


def _kind(graph, subject, predicate, obj_type, obj, params):
    g = Graph()
    if 'resource' not in obj or not obj['resource'] or obj['resource'].strip() == "":
        k = BNode()
        _apply_schema(g, k, obj, _kind_schema)
        pred = V['organization-name'] if graph.value(k, RDF.type) == V.Organization else V.fn
        if 'name' in obj and obj['name']:
            _one(g, k, pred, Literal, obj['name'], {})
    else:
        k = URIRef(obj['resource'])

    try:
        g.add( (subject, predicate, k) )
    except Exception as e:
        log.warning("[_kind] kind %s for %s of %s: %s", obj, predicate, subject, e.message)
        return

    graph += g.skolemize()


def _agent(graph, subject, predicate, obj_type, obj, params):
    g = Graph()
    if 'resource' not in obj or not obj['resource'] or obj['resource'].strip() == "":
        a = BNode()
        _apply_schema(g, a, obj, _agent_schema)
        # should set RDF.type here to FOAF.Agent if not set...
    else:
        a = URIRef(obj['resource'])

    try:
        g.set( (subject, predicate, a) )
    except Exception as e:
        log.warning("[_agent] agent %s for %s of %s: %s", obj, predicate, subject, e.message)
        return

    graph += g.skolemize()


def _spatial(graph, subject, predicate, obj_type, obj, params):
    g = Graph()
    s = BNode()
    try:
        g.set( (s, RDF.type, DCTERMS.Location) )
        g.add( (s, LOCN.geometry, Literal(obj, datatype="https://www.iana.org/assignments/media-types/application/vnd.geo+json")) )
        g.add( (subject, predicate, s) )
    except Exception as e:
        log.warning("[_spatial] spatial %s for %s of %s: %s", obj, predicate, subject, e.message)
        return

    graph += g.skolemize()


def _theme(graph, subject, predicate, obj_type, obj, params):
    _many(graph, subject, predicate, obj_type, CATEGORY[_authority_themes[obj['name']]], params)


def _checksum(graph, subject, predicate, obj_type, obj, params):
    _resource_enabled(graph, subject, predicate, obj, SPDX.Checksum, _checksum_schema)


def _period_of_time(graph, subject, predicate, obj_type, obj, params):
    _resource_enabled(graph, subject, predicate, obj, DCTERMS.PeriodOfTime, _temporal_schema)


def _frequency(graph, subject, predicate, obj_type, obj, params):
    _resource_enabled(graph, subject, predicate, obj, DCTERMS.Frequency, _labeled_schema)


def _license_document(graph, subject, predicate, obj_type, obj, params):
    _resource_enabled(graph, subject, predicate, obj, DCTERMS.LicenseDocument, _license_schema)


def _linguistic_system(graph, subject, predicate, obj_type, obj, params):
    _resource_enabled(graph, subject, predicate, obj, DCTERMS.LinguisticSystem, _labeled_schema)


def _standard(graph, subject, predicate, obj_type, obj, params):
    _resource_enabled(graph, subject, predicate, obj, DCTERMS.Standard, _labeled_schema)


def _rights_statement(graph, subject, predicate, obj_type, obj, params):
    _resource_enabled(graph, subject, predicate, obj, DCTERMS.RightsStatement, _labeled_schema)


def _provenance_statement(graph, subject, predicate, obj_type, obj, params):
    _resource_enabled(graph, subject, predicate, obj, DCTERMS.ProvenanceStatement, _labeled_schema)


def _location(graph, subject, predicate, obj_type, obj, params):
    _resource_enabled(graph, subject, predicate, obj, DCTERMS.Location, _labeled_schema)


def _format(graph, subject, predicate, obj_type, obj, params):
    g = Graph()
    try:
        if obj.startswith(FILETYPE):
            c = URIRef(obj)
        elif obj.upper() in _authority_filetypes:
            c = obj_type[obj.upper()]
        else:
            c = BNode()
            g.set( (c, RDF.type, DCTERMS.MediaTypeOrExtent) )
            g.set( (c, RDFS.label, Literal(obj)) )

            g.set( (subject, predicate, c) )
    except Exception as e:
        log.warning("[_format] format %s for %s of %s: %s", obj, predicate, subject, e.message)
        return

    graph += g.skolemize()


def _resource_enabled(graph, subject, predicate, obj, rdf_type, schema):
    g = Graph()
    if 'resource' not in obj or not obj['resource'] or obj['resource'].strip() == "":
        c = BNode()
        g.set( (c, RDF.type, rdf_type) )
        _apply_schema(g, c, obj, schema)
    else:
        if 'resource' in obj and obj['resource']:
            c = URIRef(_urlencode(obj['resource'].strip()))
        else:
            return

    try:
        g.add( (subject, predicate, c) )
    except Exception as e:
        log.warning("[_resource_enabled] resource enabled %s for %s of %s: %s", obj, predicate, subject, e.message)
        return

    graph += g.skolemize()


def _normalized_datetime(graph, subject, predicate, obj_type, obj, params):
    try:
        if obj:
            normalized = dateutil.parser.parse(obj.strip('"'))
        else:
            return
    except Exception as e:
        log.warning("[_normalized_datetime] parsed date %s: %s", obj, e.message)
        return

    try:
        graph.set( (subject, predicate, Literal(normalized.isoformat(), datatype=XSD.dateTime)) )
    except Exception as e:
        log.warning("[_normalized_datetime] normalize date %s for %s of %s: %s", obj, predicate, subject, e.message)


_temporal_schema = {
    'start_date': [_normalized_datetime, SCHEMA.startDate, Literal],
    'end_date': [_normalized_datetime, SCHEMA.endDate, Literal]
}

_agent_schema = {
    'type': [_one, RDF.type, URIRef],
    'publisher_type': [_one, DCTERMS.type, URIRef],
    'name': [_one, FOAF.name, Literal],
    'email': [_one, FOAF.mbox, URIRef, _validate_email]
}

_kind_schema = {
    'type': [_one, RDF.type, URIRef],
    # 'name': [one, V.fn, Literal],
    'email': [_one, V.hasEmail, URIRef, _validate_email]
}

_checksum_schema = {
    'algorithm': [_one, SPDX.algorithm, URIRef],
    'checksum_value': [_one, SPDX.checksumValue, Literal, {'datatype': XSD.hexBinary}]
}

_license_schema = {
    'type': [_one, DCTERMS.type, URIRef],
    'label': [_one, RDFS.label, Literal],
    'title': [_one, DCTERMS.title, Literal]
}

_labeled_schema = {
    'label': [_one, RDFS.label, Literal],
    'title': [_one, DCTERMS.title, Literal]
}

_catalog_record = {
    ########################
    # mandatory properties #
    ########################

    # dct:modified
    'metadata_modified': [_normalized_datetime, DCTERMS.modified, Literal, { 'datatype': XSD.dateTime }],

    ##########################
    # recommended properties #
    ##########################

    # dct:conformsTo
    'metadata_conforms_to': [_one, DCTERMS.conformsTo, URIRef],
    # adms:status
    # dct:issued
    'metadata_created': [_normalized_datetime, DCTERMS.issued, Literal, { 'datatype': XSD.dateTime }],

    #######################
    # optional properties #
    #######################

    # dct:description
    # dct:title
    # dct:source
    'source_record': [_one, DCTERMS.source, URIRef],
    # dct:language (multilingual/many)
    'metadata_language': [_linguistic_system, DCTERMS.language, URIRef]
}

_dataset_schema = {
    ########################
    # mandatory properties #
    ########################

    # dct:title (multilingual)
    'title': [_multilingual, DCTERMS.title, Literal],
    # dct:description (multilingual)
    'notes': [_multilingual, DCTERMS.description, Literal],

    ##########################
    # recommended properties #
    ##########################

    # dcat:contactPoint (many)
    'contact_point': [_kind, DCAT.contactPoint, URIRef],
    # dcat:keyword (many)
    'tags': [_keyword, DCAT.keyword, Literal],
    # dct:publisher
    'publisher': [_agent, DCTERMS.publisher, URIRef],
    # dcat:theme (many)
    'groups': [_theme, DCAT.theme, URIRef],

    #######################
    # optional properties #
    #######################

    # dct:conformsTo (many)
    'conforms_to': [_standard, DCTERMS.conformsTo, URIRef],
    # dct:accrualPeriodicity
    'accrual_periodicity': [_frequency, DCTERMS.accrualPeriodicity, URIRef],
    # dct:identifier (many)
    'identifier': [_many, DCTERMS.identifier, Literal],
    # dcat:landingPage
    'url': [_one, DCAT.landingPage, URIRef, _urlencode],
    # dct:language (multilingual/many)
    'language': [_linguistic_system, DCTERMS.language, URIRef],
    # adms:identifier (many)
    'other_identifier': [_many, ADMS.identifier, Literal],
    'id': [_add_one, ADMS.identifier, Literal],
    # dct:issued
    'issued': [_normalized_datetime, DCTERMS.issued, Literal],
    # dct:spatial (many)
    # 'spatial': [_spatial, DCTERMS.spatial, URIRef],
    'dcat_spatial': [_location, DCTERMS.spatial, URIRef],
    # dct:temporal
    'temporal': [_period_of_time, DCTERMS.temporal, URIRef],
    # dct:modified
    'modified': [_normalized_datetime, DCTERMS.modified, Literal],
    # owl:versionInfo
    'version_info': [_one, OWL.versionInfo, Literal],
    # adms:versionNotes (multilingual)
    'version_notes': [_multilingual, ADMS.versionNotes, Literal],
    # dct:provenance (many)
    'provenance': [_provenance_statement, DCTERMS.provenance, URIRef],
    # dct:source (many)
    'source': [_many, DCTERMS.source, URIRef],
    # dct:accessRights (many)
    'access_rights': [_rights_statement, DCTERMS.accessRights, URIRef],
    # dct:hasVersion (many)
    'has_version': [_many, DCTERMS.hasVersion, URIRef],
    # dct:isVersionOf (many)
    'is_version_of': [_many, DCTERMS.isVersionOf, URIRef],
    # dct:relation (many)
    'relation': [_many, DCTERMS.relation, URIRef],
    # foaf:page (many)
    'page': [_many, FOAF.page, URIRef, _urlencode],
    # adms:sample (many)
    'sample': [_many, ADMS.sample, URIRef],
    # dct:type
    'dataset_type': [_one, DCTERMS.type, URIRef]
}

_distribution_schema = {
    ########################
    # mandatory properties #
    ########################

    # dcat:accessURL
    'url': [_many, DCAT.accessURL, URIRef, _urlencode],

    ##########################
    # recommended properties #
    ##########################

    # dct:description (multilingual)
    'description': [_multilingual, DCTERMS.description, Literal],
    # dct:format
    'format': [_format, DCTERMS['format'], FILETYPE],
    # dct:licence
    'licence': [_license_document, DCTERMS.licence, URIRef],

    #######################
    # optional properties #
    #######################

    # dcat:byteSize
    'size': [_one, DCAT.byteSize, Literal, {'datatype': XSD.decimal}],
    # spdx:checksum
    'checksum': [_checksum, SPDX.checksum, URIRef],
    # dcat:downloadURL
    'download_url': [_many, DCAT.downloadURL, URIRef, _urlencode],
    # dcat:mediaType (IANA)
    'mimetype': [_one, DCAT.mediaType, Literal],
    # dct:issued
    'issued': [_normalized_datetime, DCTERMS.issued, Literal],
    # dct:rights
    'rights': [_rights_statement, DCTERMS.rights, URIRef],
    # adms:status
    'status': [_one, ADMS.status, URIRef],
    # dct:title (multilingual)
    'name': [_multilingual, DCTERMS.title, Literal],
    # dct:modified
    'modified': [_normalized_datetime, DCTERMS.modified, Literal],
    # dct:conformsTo (many)
    'conforms_to': [_standard, DCTERMS.conformsTo, URIRef],
    # foaf:page (many)
    'page': [_many, FOAF.page, URIRef, _urlencode],
    # dct:language (multilingual/many)
    'language': [_linguistic_system, DCTERMS.language, URIRef]
}

_catalog_schema = {
    ########################
    # mandatory properties #
    ########################
    # dct:description (multilingual)
    'description': [_multilingual, DCTERMS.description, Literal],
    # dct:publisher
    'publisher': [_agent, DCTERMS.publisher, URIRef],
    # dct:title (multilingual)
    'title': [_multilingual, DCTERMS.title, Literal],

    ##########################
    # recommended properties #
    ##########################
    # foaf:homepage
    'homepage': [_one, FOAF.homepage, URIRef, _urlencode],
    # dct:language (multilingual/many)
    'language': [_linguistic_system, DCTERMS.language, URIRef],
    # dct:license
    'license': [_license_document, DCTERMS.license, URIRef],
    # dct:issued
    'created': [_normalized_datetime, DCTERMS.issued, Literal],
    # dcat:themeTaxonomy (many)
    # dct:modified

    #######################
    # optional properties #
    #######################
    # dct:rights
    'rights': [_rights_statement, DCTERMS.rights, URIRef],
    # dct:spatial (many)
    'spatial': [_many, DCTERMS.spatial, URIRef]
    # dct:isPartOf (one)
    # dct:hasPart (many)
}


def _apply_schema(graph, subject, obj_dict, schema):
    for key, value in schema.items():
        field = get_field(obj_dict, key)

        if isinstance(field, basestring) and field.strip() == "":
            continue

        if field:
            call = value[0]
            predicate = value[1]
            obj_type = value[2]
            params = value[3] if len(value) > 3 else {}

            if isinstance(field, list):
                for elem in field:
                    if elem:
                        call(graph, subject, predicate, obj_type, elem, params)
            else:
                call(graph, subject, predicate, obj_type, field, params)


def dataset(graph, package):
    """
    Create all triples for a dcat ap dataset and inserts them into a graph.
    :param graph: The graph storing the dataset
    :type graph: Graph
    :param package: The ckan package as dictionary
    :type package: dict
    :return: Reference of the generated dataset resource
    :rtype: URIRef
    """

    dset = DATASET[package['name']]

    graph.set( (dset, RDF.type, DCAT.Dataset) )

    _apply_schema(graph, dset, package, _dataset_schema)

    if (dset, DCTERMS.contactPoint, None) not in graph:
        if 'maintainer' in package and package['maintainer']:
            maintainer = {'type': V.Kind, 'name': package['maintainer']}
            if 'maintainer_email' in package and package['maintainer_email']:
                maintainer.update({'mail': package['maintainer_email']})
            _kind(graph, dset, DCAT.contactPoint, URIRef, maintainer, {})

    if (dset, DCTERMS.publisher, None) not in graph:
        if 'author' in package and package['author']:
            author = {'type': FOAF.Agent, 'name': package['author']}
            if 'author_email' in package and package['author_email']:
                author.update({'mail': package['author_email']})
            _agent(graph, dset, DCTERMS.publisher, URIRef, author, {})

    spatial = get_extra_field(package, 'spatial', None)
    if spatial and spatial.strip() != "":
        _spatial(graph, dset, DCTERMS.spatial, URIRef, spatial, {})

    translation = get_field(package, 'translation', None)
    translation_meta = get_field(package, 'translation_meta', None)
    if translation_meta:
        if 'default' in translation_meta:
            default = translation_meta['default']
        for lang in translation_meta['languages']:
            tag = translation_meta[lang]['tag']
            if translation and lang in translation and tag != 'processing':
                values = translation[lang]
                title = get_field(values if default != lang else package, 'title', None)
                if title:
                    graph.add( (dset, DCTERMS.title, Literal(title, lang=tag)) )
                description = get_field(values if default != lang else package, 'notes', None)
                if description:
                    graph.add( (dset, DCTERMS.description, Literal(description, lang=tag)) )

    return dset


def catalog_record(graph, dataset, package):
    """
    Do generate all triples defining a dcat ap catalog record for a specific dataset
    :param graph: Graph for storing the triples
    :type graph: Graph
    :param dataset: The belonging dataset
    :type dataset: URIRef
    :param package: The ckan package as dictionary defining the dataset
    :type package: dict
    :return: Reference of the generated catalog resource
    :rtype: URIRef
    """
    record = RECORD[package['name']]

    graph.set( (record, RDF.type, DCAT.CatalogRecord) )

    _apply_schema(graph, record, package, _catalog_record)

    if (record, DCTERMS.modified, None) not in graph:
        graph.set( (record, ADMS.status, URIRef(':created')) )
    else:
        graph.set( (record, ADMS.status, URIRef(':modified')) )

    # mandatory properties
    graph.set( (record, FOAF.primaryTopic, dataset) )

    return record


def distribution(graph, package, resource):
    """
    Do generate all triples defining a dcat ap distribution for a specific dataset
    :param graph: Graph for storing the triples
    :type graph: Graph
    :param package: The ckan package that the distribution belongs to
    :type package: dict
    :param resource: The ckan resource as dictionary defining the distribution
    :type resource: dict
    :return: Reference of the generated distribution resource
    :rtype: URIRef
    """
    dist = DISTRIBUTION[resource['id']]
    graph.add( (dist, RDF.type, DCAT.Distribution) )

    _apply_schema(graph, dist, resource, _distribution_schema)

    if (dist, DCTERMS.license, None) not in graph:
        if 'license_url' in package:
            graph.add( (dist, DCTERMS.license, URIRef(package['license_url'])) )

    translation = get_field(resource, 'translation', None)
    translation_meta = get_field(package, 'translation_meta', None)
    if translation_meta:
        if 'default' in translation_meta:
            default = translation_meta['default']
        for lang in translation_meta['languages']:
            tag = translation_meta[lang]['tag']
            if translation and lang in translation and tag != 'processing':
                values = translation[lang]
                title = get_field(values if default != lang else package, 'name', None)
                if title:
                    graph.add( (dist, DCTERMS.title, Literal(title, lang=tag)) )
                description = get_field(values if default != lang else package, 'description', None)
                if description:
                    graph.add( (dist, DCTERMS.description, Literal(description, lang=tag)) )

    return dist


def catalog(graph, organization):
    """
    Generate all triples defining a dcat ap catalog and inserts them into a graph
    :param graph: Graph for storing the triples
    :type graph: Graph
    :param organization: The publishing organization of the catalog
    :type organization: dict
    :return: The reference of the generated catalog resource
    :rtype: URIRef
    """
    c = CATALOG[organization['name']]
    graph.add( (c, RDF.type, DCAT.Catalog) )

    _apply_schema(graph, c, organization, _catalog_schema)

    # dcat:themeTaxonomy
    graph.add( (c, DCAT.themeTaxonomy, URIRef(THEME)) )

    translation = get_field(organization, 'translation', None)
    translation_meta = get_field(organization, 'translation_meta', None)
    if translation_meta:
        if 'default' in translation_meta:
            default = translation_meta['default']
        for lang in translation_meta['languages']:
            tag = translation_meta[lang]['tag']
            if translation and lang in translation and tag != 'processing':
                values = translation[lang]
                title = get_field(values if default != lang else organization, 'title', None)
                if title:
                    graph.add( (c, DCTERMS.title, Literal(title, lang=tag)) )
                description = get_field(values if default != lang else organization, 'description', None)
                if description:
                    graph.add( (c, DCTERMS.description, Literal(description, lang=tag)) )

    return c
