# -*- coding: utf-8 -*-

import sys
import timeit
import time
import json

from SPARQLWrapper.SPARQLExceptions import EndPointNotFound

from ckan import model
from ckan.logic import get_action, NotFound

import ckan.plugins.toolkit as tk
import ckan.logic.action.get as get

from ckanext.eodp.sparql_store.graph import PanEODP
from ckanext.eodp.sparql_store.dcatap import DATASET, CATALOG, DCAT
from ckanext.eodp.sparql_store.crud import update_dataset, create_dataset, delete_dataset, update_organization

from ckan.lib.cli import CkanCommand

__author__ = 'sim'


class SparqlSync(CkanCommand):
    """
    Synchronize with a triple store via SPARQL

    Usage:

    The commands should be run from the ckanext-eodp directory and expect
    a development.ini file to be present. Most of the time you will
    specify the config explicitly though::

        paster sparqlsync update all --config=../ckan/development.ini

    """

    summary = __doc__.split('\n')[0]
    usage = __doc__
    max_args = 9
    min_args = 0

    def __init__(self, name):

        super(SparqlSync, self).__init__(name)

    def command(self):
        self._load_config()

        # We'll need a sysadmin user to perform most of the actions
        # We will use the sysadmin site user (named as the site_id)
        context = {'model': model, 'session': model.Session, 'ignore_auth': True}
        self.admin_user = get_action('get_site_user')(context, {})

        print ''

        if len(self.args) == 0:
            self.parser.print_usage()
            sys.exit(1)

        cmd = self.args[0]
        if cmd == 'update':
            if len(self.args) > 1:
                if self.args[1] == 'all':
                    self.update_all(context)
                else:
                    self.update(context, self.args[1])
            else:
                print "Command update requires an organization name or 'all'"
        elif cmd == 'report':
            self.report(context)
        elif cmd == 'clean':
            print "clean"
        else:
            print "Command %s not recognized" % cmd

    def _load_config(self, load_site_user=True):
        super(SparqlSync, self)._load_config()

    def update_all(self, context):
        org_list = get.organization_list(context, {'sort': "package_count asc"})
        for organization in org_list:
            print ''
            print "sync catalogue %s" % organization
            self.update(context, organization)

    def update(self, context, organization):
        package_search = get_action('package_search')
        package_show = get_action('package_show')
        organization_show = get_action('organization_show')

        search = "organization:" + organization

        offset = 0
        updates = 0
        errors = 0
        creations = 0
        deletions = 0

        update_organization(organization_show(context, {'id': organization}))

        paneodp = PanEODP()

        startoverall = timeit.default_timer()

        print "deleting...",
        sys.stdout.flush()

        cng = paneodp.get_context(CATALOG[organization])
        for s in cng.objects(CATALOG[organization], DCAT.dataset):
            for i in xrange(1, 4):
                name = s.rpartition('/')[2]
                try:
                    try:
                        result = package_show(context, {'id': name})
                        if result['success']:
                            package = result['result']
                            if 'state' in package and package['state'] == 'deleted':
                                delete_dataset(name, organization)
                                deletions += 1
                        elif result['error']['message'] == 'Not found':
                                delete_dataset(name, organization)
                                deletions += 1
                    except NotFound:
                        delete_dataset(name, organization)
                        deletions += 1
                    print "\rdeleting... %s datasets" % deletions,
                except EndPointNotFound:
                    print "\rendpoint not reachable, waiting 2 seconds and trying again...",
                    time.sleep(2)
                    print "\rdeleting... %s datasets" % deletions,

        print "\rdeletion done: %s datasets in %.1f seconds" % (deletions, round(timeit.default_timer() - startoverall,
                                                                                 1))

        search_result = package_search(context, {'q': search, 'rows': 0, 'start': 0})
        if 'count' in search_result:
            print "sync %s packages" % search_result['count']
            print "0 datasets updated, 0 datasets created, 0 errors (no time taken yet)",

            count = 500

            startoverall = timeit.default_timer()

            while count >= 500:
                search_result = package_search(context, {'q': search, 'rows': 500, 'start': offset})
                results = search_result['results']
                count = len(results)

                for package in results:
                    for i in xrange(1, 4):
                        try:
                            ng = paneodp.get_context(DATASET[package['name']])
                            if len(ng) > 0:
                                if update_dataset(package):
                                    updates += 1
                                else:
                                    errors += 1
                            else:
                                if create_dataset(package):
                                    creations += 1
                                else:
                                    errors += 1
                            break
                        except EndPointNotFound:
                            print "endpoint not reachable, waiting 2 seconds and trying again..."
                            time.sleep(2)
                        except Exception as e:
                            print e.message.decode('utf-8')
                            errors += 1
                            break

                print "\r                                                                                             ",
                print "\r%s datasets updated, %s datasets created, %s errors (time taken %.1f seconds)" \
                      % (updates, creations, errors, round(timeit.default_timer() - startoverall, 1)),
                sys.stdout.flush()

                offset += 500

        else:
            print "search failed: %s" % json.dumps(search_result, indent=3)

    def report(self, context):
        organization_show = get_action('organization_show')
        paneodp = PanEODP()

        result = []

        datasets = paneodp.query("""SELECT COUNT(*) WHERE { ?s a dcat:Dataset }""")
        for count in datasets:
            print "%s datasets in store" % count

        org_list = get.organization_list(context, {'sort': "package_count asc"})
        for organization in org_list:
            org = organization_show(context, {'id': organization})
            ng = paneodp.get_context(CATALOG[organization])
            if len(ng) == 0:
                print "%s: missing" % organization
                result.append({'catalog': organization, 'ckan': org['package_count'], 'rdf': None})
            else:
                datasets = ng.query("select count(?o) where {?s dcat:dataset ?o}")
                for count in datasets:
                    result.append({'catalogue': organization, 'ckan': org['package_count'], 'rdf': count[0].toPython()})

        print json.dumps(result, indent=3)
