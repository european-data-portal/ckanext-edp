from schemaorg import package_to_schema
import json


def get_schemaorg_snippet(pkg):
    try:
        schema = package_to_schema(pkg)
        return '<script type="application/ld+json">\n' + json.dumps(schema, indent=2) + '\n</script>'
    except:
        return ''

