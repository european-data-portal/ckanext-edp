from ckanext.eodp.config import CONFIG
from ckan.model.package import Package
import urllib

import logging

log = logging.getLogger(__name__)

base_url = CONFIG.get('license_assistant', 'url')


def get_license_assistant_data(license_id):
    license = get_license(license_id)
    if license:
        return {
            'link': license.url
        }
    else:
        return None


def get_license(license_id):
    licenses = Package.get_license_register()
    try:
        return licenses[license_id]
    except KeyError:
        return False
    except Exception:
        return False


def _build_link(license_id):
    """
    :type pkg_dict: dict
    :type format_name: str
    :rtype: str
    :param pkg_dict:
    :param format:
    :return:
    """
    parameters = {
        'license_id': license_id
    }
    return base_url + '?' + urllib.urlencode(parameters)



