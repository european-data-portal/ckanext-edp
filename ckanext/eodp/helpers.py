# -*- coding: utf-8 -*-
__author__ = 'sim'

from pylons import config
from ckanext.eodp.config import CONFIG
from ckanext.eodp.maps import pkg_is_geo_enabled
from ckanext.eodp.visualisation import pkg_visualisation_enabled
from ckanext.eodp.model import CountryOrganization
from ckan.lib.helpers import lang, url_for_static, get_facet_items_dict
from ckan.lib.helpers import full_current_url, _url_with_params, _create_url_with_params
from ckan.lib.helpers import follow_button as follow_button_super
import ckan.plugins.toolkit as tk
from ckan.common import _, g
import urllib2
import translation
import pycountry
import ckan.model as model
import json
import logging
import copy
from ckan.lib.helpers import snippet
log = logging.getLogger(__name__)


def get_extra_field(obj_dict, key, default):
    """
    This is more or less the ckan helper function 'get_pkg_dict_extra' in a more generalized form
    :param obj_dict: dictionary with extras
    :param key: extra key to lookup
    :param default: default value returned if not found
    :return: extra value
    """
    extras = obj_dict['extras'] if 'extras' in obj_dict else []

    for extra in extras:
        if extra['key'] == key:
            return extra['value']

    return default


def get_field(obj_dict, key, default=None):
    return obj_dict[key] if key in obj_dict and obj_dict[key] else default


def language(context):
    """

    :param context:
    :return:
    """

    if 'request' in context:
        lang = context['request'].environ['CKAN_LANG']
    else:
        lang = config.get('ckan.locale_default', 'en')

    return lang


def build_language_link(link, trailing=False):
    if trailing:
        return link + lang() + '/'
    else:
        return '/' + lang() + link


def unquote_url(url):
    return urllib2.unquote(url)


def feedback_button(pkg):

    _ = tk._

    def build_parameters(par_dict):
        result_list = []
        for e in par_dict:
            par = e + '=' + par_dict[e].replace(' ', '+')
            result_list.append(par)
        return '&'.join(result_list)

    parameters = {
        'summary': _('Feedback about Dataset') + ': ' + pkg['title'],
        'type': '3',
        'description': _('Dataset URL') + ': ' + full_current_url()
    }

    link = '/' + lang() + '/feedback/form' + '?' + build_parameters(parameters)
    return tk.render_snippet('package/snippets/feedback_button.html', {'link': link})


def preview_available(pkg):
    geo = pkg_is_geo_enabled(pkg)
    visu = pkg_visualisation_enabled(pkg)
    if geo or visu:
        return True
    else:
        return False


def _get_spatial_list():
    query = model.Session.query(model.GroupExtra)
    query = query.filter(model.GroupExtra.key == 'spatial')
    extras = query.all()
    result = set()
    for e in extras:
        try:
            parsed = json.loads(e.value)
            for p in parsed:
                result.add(p)
        except:
            pass

    result.add(u'EU')
    return list(result)


def get_available_countries():
    spatial = _get_spatial_list()
    result = []
    for s in spatial:
        if ('spatial', s) in tk.request.params.items():
            active = True
        else:
            active = False
        if s == u'EU':
            display_name = _('European Catalogues')
            result.append({
                'display_name': display_name,
                'name': s,
                'active': active
            })
        else:
            display_name = pycountry.countries.get(alpha2=s).name
            result.append({
                'display_name': translation.get_country_translation(display_name).decode('utf-8'),
                'name': s,
                'active': active
            })

    return sorted(result, key=lambda k: k['display_name'])


def get_organization_image_url(org_dict, extras_included=True):

    if not extras_included:
        spatial_query = model.Session.query(model.GroupExtra) \
            .filter(model.GroupExtra.group_id == org_dict['id']) \
            .filter(model.GroupExtra.key == 'spatial') \
            .all()

        if len(spatial_query) == 1:
            try:
                spatial_value = json.loads(spatial_query[0].value)
                org_dict['spatial'] = spatial_value
            except:
                pass
        # context = {}
        # org_dict = tk.get_action('organization_show')(context, {
        #     'id': org_dict['id']
        # })

    try:
        spatial = org_dict['spatial']
    except KeyError:
        return url_for_static('/img/flags/europe.png')

    if not isinstance(spatial, list):
        return None

    if not spatial or len(spatial) > 1:
        return url_for_static('/img/flags/europe.png')
    else:
        country = spatial[0].lower()
        file_path = '/img/flags/%s.png' % country
        return url_for_static(file_path)


def get_main_menu_link(link):
    l = lang()
    if l:
        return '/' + l + link
    else:
        return '/en' + link


def get_country_facet(static_facets):
    org_facet = copy.deepcopy(static_facets['organization'])
    result_dict = {}
    result_facet = []
    if org_facet:
        for f in org_facet:
            country = _get_country_for_org_by_name(f['name'])
            if country:
                if country == 'EU':
                    display_name = _('European Catalogues')
                else:
                    display_name = pycountry.countries.get(alpha2=country).name
                    display_name = translation.get_country_translation(display_name).decode('utf-8')

                f['display_name'] = display_name
                f['name'] = country.lower()
                if country in result_dict:
                    result_dict[country]['count'] = result_dict[country]['count'] + f['count']
                else:
                    result_dict[country] = f

        request_params = tk.request.params.items()

        for key, value in result_dict.items():
            if ('country', value['name']) in request_params:
                value['active'] = True
            else:
                value['active'] = False
            result_facet.append(value)

        return result_facet
    else:
        return []


def get_static_facets(grp=None, org=None):
    package_search = tk.get_action('package_search')

    fq = '+dataset_type:dataset'
    q = u''
    if grp:
        fq = fq + ' groups:' + grp

    if org:
        q = fq + ' organization:' + org

    data_dict = {
        'q': q,
        'fq': fq,
        'facet.field': ['organization', 'groups', 'tags', 'res_format', 'license_id'],
        'rows': 1,
        'start': 0,
        'sort': None,
        'extras': {'before_search': False}
    }

    pkgs = package_search({}, data_dict)
    result = pkgs['search_facets']
    facets = {}
    more_facets = {}
    for key, value in result.items():
        facets[key] = []
        for item in value['items']:
            if not len(item['name'].strip()):
                continue
            if not (key, item['name']) in tk.request.params.items():
                facets[key].append(dict(active=False, **item))
            else:
                facets[key].append(dict(active=True, **item))

        facets[key] = sorted(facets[key], key=lambda i: i['count'], reverse=True)

        if key == 'organization':
            facets['country'] = get_country_facet(facets)
            facets['country'] = sorted(facets['country'], key=lambda i: i['count'], reverse=True)
            try:
                country_limit = int(tk.request.params.get('_country_limit', g.facets_default_number))
            except ValueError:
                country_limit = 10
            if country_limit > 0 and len(facets['country']) > country_limit:
                more_facets['country'] = True


        if tk.c.search_facets_limits:
            limit = tk.c.search_facets_limits.get(key)
        else:
            limit = 10
        if limit is not None and limit > 0 and len(facets[key]) > limit:
            more_facets[key] = True
        else:
            more_facets[key] = False
    return facets, more_facets


def _get_country_for_org(org_id):

    spatial_query = model.Session.query(model.GroupExtra) \
        .filter(model.GroupExtra.group_id == org_id) \
        .filter(model.GroupExtra.key == 'spatial') \
        .all()

    if len(spatial_query) == 1:
        return json.loads(spatial_query[0].value)
    else:
        return None


def _get_country_for_org_by_name(name):
    spatial_query = model.Session.query(CountryOrganization)\
        .filter(CountryOrganization.org_name == name)\
        .all()

    if len(spatial_query) == 1:
        return spatial_query[0].country
    else:
        return None


def country_name_by_param(code):
    code = code.upper()
    if code == 'EU':
        display_name = _('European Catalogues')
    else:
        display_name = pycountry.countries.get(alpha2=code).name
        display_name = translation.get_country_translation(display_name).decode('utf-8')
    return display_name


def country_remove_url_param(key, value=None, replace=None, controller=None,
                     action=None, extras=None, alternative_url=None):
    if isinstance(key, basestring):
        keys = [key]
    else:
        keys = key

    if value:
        value = value.split(',')[0]

    params_nopage = [(k, v) for k, v in tk.request.params.items() if k != 'page']
    params = list(params_nopage)
    for k, v in params:
        if k == key and v.startswith(value + ','):
            params.remove((keys[0], v))
    if replace is not None:
        params.append((keys[0], replace))

    if alternative_url:
        return _url_with_params(alternative_url, params)

    return _create_url_with_params(params=params, controller=controller,
                                   action=action, extras=extras)


def follow_button(obj_type, obj_id):
    button = follow_button_super(obj_type, obj_id)
    if len(button) > 0:
        return button
    else:
        return snippet('snippets/follow_button_login.html', pkg_id=obj_id)


def to_json(obj):
    return json.dumps(obj)


