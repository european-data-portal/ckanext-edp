import ckan.plugins as plugins
import ckan.plugins.toolkit as tk
from ckan.lib.plugins import DefaultOrganizationForm
from ckanext.eodp.schema.helper import *
from routes.mapper import SubMapper
import logging
log = logging.getLogger(__name__)


class OrganizationPlugin(plugins.SingletonPlugin, DefaultOrganizationForm):

    plugins.implements(plugins.IGroupForm, inherit=False)
    plugins.implements(plugins.IRoutes, inherit=False)

    def before_map(self, map):
        with SubMapper(map, controller='organization') as m:
            # This is workaround for a bug in the CKAN routing
            # The dataset tab in the organization view is not rendered correctly
            # so we have to redefine the original route here
            m.connect('/organization/new', action='new')
            m.connect('organization_read', '/organization/{id}', action='read')
            m.connect('organization_read', '/organization/{id}', action='read', ckan_icon='sitemap')
            m.connect('organization_activity', '/organization/activity/{id}',
              action='activity', ckan_icon='time')
        return map

    def after_map(self, map):
        with SubMapper(map, controller='organization') as m:
            # This is workaround for a bug in the CKAN routing
            # The dataset tab in the organization view is not rendered correctly
            # so we have to redefine the original route here
            m.connect('/organization/new', action='new')
            m.connect('organization_read', '/organization/{id}', action='read')
            m.connect('organization_read', '/organization/{id}', action='read', ckan_icon='sitemap')
            m.connect('organization_activity', '/organization/activity/{id}',
                      action='activity', ckan_icon='time')
        return map

    def is_fallback(self):
        return False

    def db_to_form_schema(self, package_type=None):
        from ckan.logic.schema import default_group_schema
        schema = dcat_schema.dcat_ap_show_organization_schema()
        schema.update(default_group_schema())
        schema.update(self.__fixSchema())
        return schema

    def form_to_db_schema_api_update(self):
        return self.form_to_db_schema()

    def form_to_db_schema_api_create(self):
        return self.form_to_db_schema()

    def form_to_db_schema(self):
        from ckan.logic.schema import group_form_schema
        schema = dcat_schema.dcat_ap_modify_organization_schema()
        schema.update(group_form_schema())
        # schema.update(self.__fixSchema())
        return schema

    def group_types(self):
        return ['organization']

    def __fixSchema(self):
        # Workaround for missing fields in template
        fixschema = {
            'package_count': [tk.get_validator('ignore_missing')],
            'num_followers': [tk.get_validator('ignore_missing')],
            'display_name': [tk.get_validator('ignore_missing')],
            'created': [tk.get_validator('ignore_missing')]
        }
        return fixschema

    def group_controller(self):
        return 'organization'

