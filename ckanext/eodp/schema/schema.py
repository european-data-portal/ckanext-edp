__author__ = 'fki'


import ckan.plugins.toolkit as tk
from ckanext.eodp.schema import converters, validators
from ckanext.eodp.schema.converters import (
    convert_from_json,
    convert_nested_list_to_json,
    convert_to_json,
    convert_list_to_extras,
    null_to_empty_sting)
from ckanext.eodp.translation import TRANSFIELD
from ckanext.eodp.trans import METAFIELD

ign_missing = tk.get_validator('ignore_missing')
url_validator = tk.get_validator('url_validator')
to_extras = tk.get_converter('convert_to_extras')
from_extras = tk.get_converter('convert_from_extras')
emp_to_none = converters.empty_string_to_none
not_empty = tk.get_validator('not_empty')


def dcat_ap_modify_package_schema():

    contact_point = {
        'type': [ign_missing, convert_list_to_extras],
        'name': [ign_missing, convert_list_to_extras],
        'email': [ign_missing, convert_list_to_extras],
        'resource': [ign_missing, convert_list_to_extras]
    }

    temporal = {
        'start_date': [ign_missing, convert_list_to_extras],
        'end_date': [ign_missing, convert_list_to_extras],
    }

    provenance = {
        'label': [ign_missing, convert_list_to_extras],
        'resource': [ign_missing, convert_list_to_extras]
    }

    conforms_to = {
        'label': [ign_missing, convert_list_to_extras],
        'resource': [ign_missing, convert_list_to_extras]
    }

    language = {
        'label': [ign_missing, convert_list_to_extras],
        'resource': [ign_missing, convert_list_to_extras]
    }

    dcat_spatial = {
        'label': [ign_missing, convert_list_to_extras],
        'resource': [ign_missing, convert_list_to_extras]
    }

    metadata_language = {
        'label': [ign_missing, convert_list_to_extras],
        'resource': [ign_missing, convert_list_to_extras]
    }

    schema = {
        'notes': [null_to_empty_sting, validators.is_string, unicode],
        'contact_point': contact_point,
        'publisher': [emp_to_none, ign_missing, convert_from_json, to_extras],
        'access_rights': [emp_to_none, ign_missing, convert_from_json, to_extras],
        'conforms_to': conforms_to,
        'page': [emp_to_none, ign_missing, convert_from_json, to_extras],
        'accrual_periodicity': [emp_to_none, ign_missing, convert_from_json, to_extras],
        'has_version': [emp_to_none, ign_missing, convert_from_json, to_extras],
        'is_version_of': [emp_to_none, ign_missing, convert_from_json, to_extras],
        'identifier': [emp_to_none, ign_missing, validators.list_of_strings, convert_from_json, to_extras],
        'language': language,
        'other_identifier': [emp_to_none, ign_missing, convert_from_json, to_extras],
        'provenance': provenance,
        'relation': [emp_to_none, ign_missing, convert_from_json, to_extras],
        'issued': [emp_to_none, ign_missing, to_extras],
        'sample': [emp_to_none, ign_missing, convert_from_json, to_extras],
        'source': [emp_to_none, ign_missing, convert_from_json, to_extras],
        'dcat_spatial': dcat_spatial,
        'dct_type': [emp_to_none, ign_missing, to_extras],
        'temporal': temporal,
        'modified': [emp_to_none, ign_missing, to_extras],
        'version_info': [emp_to_none, ign_missing, to_extras],
        'version_notes': [emp_to_none, ign_missing, to_extras],
        'original_source': [ign_missing, url_validator, to_extras],
        'metadata_conforms_to': [emp_to_none, ign_missing, to_extras],
        'metadata_language': metadata_language,
        'translated': [emp_to_none, ign_missing, to_extras],
        METAFIELD: [emp_to_none, ign_missing, convert_from_json, to_extras],
        TRANSFIELD: [emp_to_none, ign_missing, convert_from_json, to_extras],
    }

    return schema


def dcat_ap_modify_resource_schema():

    schema = {
        'access_url': [ign_missing, convert_from_json],
        'license': [ign_missing, convert_from_json],
        'checksum': [ign_missing, convert_from_json],
        'download_url': [ign_missing, convert_from_json],
        'issued': [ign_missing],
        'status': [ign_missing, convert_from_json],
        'modified': [ign_missing],
        'rights': [ign_missing, convert_from_json],
        'page': [ign_missing, convert_from_json],
        'conforms_to': [emp_to_none, ign_missing, convert_from_json],
        'language': [emp_to_none, ign_missing, convert_from_json],
        TRANSFIELD: [emp_to_none, ign_missing, convert_from_json],
    }
    return schema


def dcat_ap_modify_organization_schema():

    schema = {
        'description': [unicode],
        'title': [unicode],
        'publisher': [not_empty, convert_from_json, to_extras],
        'homepage': [emp_to_none, ign_missing, to_extras],
        'language': [emp_to_none, ign_missing, convert_from_json, to_extras],
        'license': [emp_to_none, ign_missing, convert_from_json, to_extras],
        'created': [emp_to_none, ign_missing, to_extras],
        'modified': [emp_to_none, ign_missing, to_extras],
        'spatial': [emp_to_none,
                    ign_missing,
                    validators.validate_org_spatial,
                    convert_from_json,
                    to_extras]
    }
    return schema


def dcat_ap_show_package_schema():

    schema = {
        'contact_point': [from_extras, ign_missing, convert_to_json],
        'publisher': [from_extras, ign_missing, convert_to_json],
        'access_rights': [from_extras, ign_missing, convert_to_json],
        'conforms_to': [from_extras, ign_missing, convert_to_json],
        'page': [from_extras, ign_missing, convert_to_json],
        'accrual_periodicity': [from_extras, ign_missing, convert_to_json],
        'has_version': [from_extras, ign_missing, convert_to_json],
        'is_version_of': [from_extras, ign_missing, convert_to_json],
        'identifier': [from_extras, ign_missing, convert_to_json],
        'language': [from_extras, ign_missing, convert_to_json],
        'other_identifier': [from_extras, ign_missing, convert_to_json],
        'provenance': [from_extras, ign_missing, convert_to_json],
        'relation': [from_extras, ign_missing, convert_to_json],
        'issued': [from_extras, ign_missing],
        'sample': [from_extras, ign_missing, convert_to_json],
        'source': [from_extras, ign_missing, convert_to_json],
        'dcat_spatial': [from_extras, ign_missing, convert_to_json],
        'dct_type': [from_extras, ign_missing],
        'temporal': [from_extras, ign_missing, convert_to_json],
        'modified': [from_extras, ign_missing],
        'version_info': [from_extras, ign_missing],
        'version_notes': [from_extras, ign_missing],
        'original_source': [from_extras, ign_missing],
        'metadata_conforms_to': [from_extras, ign_missing],
        'metadata_language': [from_extras, ign_missing, convert_to_json],
        'translated': [from_extras, ign_missing],
        METAFIELD: [from_extras, ign_missing, convert_to_json],
        TRANSFIELD: [from_extras, ign_missing, convert_to_json],
    }

    return schema


def dcat_ap_show_resource_schema():

    schema = {
        'access_url': [ign_missing, convert_to_json],
        'license': [ign_missing, convert_to_json],
        'checksum': [ign_missing, convert_to_json],
        'download_url': [ign_missing, convert_to_json],
        'issued': [ign_missing],
        'status': [ign_missing, convert_to_json],
        'modified': [ign_missing],
        'rights': [ign_missing, convert_to_json],
        'page': [ign_missing, convert_to_json],
        'conforms_to': [ign_missing, convert_nested_list_to_json],
        'language': [ign_missing, convert_nested_list_to_json],
        TRANSFIELD: [ign_missing, convert_to_json],
    }

    return schema


def dcat_ap_show_organization_schema():

    schema = {
        'publisher': [from_extras, ign_missing, convert_to_json],
        'homepage': [from_extras, ign_missing],
        'language': [from_extras, ign_missing, convert_nested_list_to_json],
        'license': [from_extras, ign_missing, convert_to_json],
        'created': [from_extras, ign_missing],
        'modified': [from_extras, ign_missing],
        'spatial': [from_extras, ign_missing, convert_to_json],
    }
    return schema