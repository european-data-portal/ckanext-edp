__author__ = 'fki'

import ckanext.eodp.util as u
import json
import datetime
from webhelpers.html.tools import strip_tags
from ckanext.eodp.schema.schema import dcat_ap_show_package_schema, dcat_ap_show_resource_schema, dcat_ap_show_organization_schema
from converters import convert_list_to_nested_list
import ckanext.eodp.schema.schema as dcat_schema
from ckanext.eodp.schema.converters import convert_to_json, convert_from_json
from ckanext.eodp.sparql_store import helpers as rdf_helpers
from ckan.lib import munge
from ckanext.eodp.translation import TRANSFIELD
import ckan.plugins.toolkit as tk

tag_name_validator = tk.get_validator('tag_name_validator')
tag_length_validator = tk.get_validator('tag_length_validator')

import logging
log = logging.getLogger(__name__)

DICTS = ['temporal_coverage']


def convert_dict_for_view(context, data_dict):
    if not u.is_api_call(context):
        [flatten_keys_in_dict(e, data_dict) for e in DICTS]


def convert_dict_from_view(context, data_dict):
    if not u.is_api_call(context):
        [unflatten_keys_to_dict(e, data_dict) for e in DICTS]


def flatten_keys_in_dict(key, data_dict):
    if key in data_dict:
        value = data_dict[key]
        #del data_dict[key]
        for k in value:
            data_dict[key + '_' + k] = value[k]


def unflatten_keys_to_dict(key, data_dict):
    result = {}
    for k in data_dict.keys():
        if k.startswith(key):
            value = k.replace(key + '_', '')
            result[value] = data_dict[k]
            #del data_dict[k]
    if result:
        data_dict[key] = result


def prepare_schema_for_view(data_dict):
    schema = dcat_schema.dcat_ap_show_package_schema()
    for field in schema.keys():
        if convert_to_json in schema[field]:
            if field in data_dict:
                data_dict[field] = json.dumps(data_dict[field])


def _prepare_schema_by_view(data_dict, schema):
    for field in schema.keys():
        if convert_from_json in schema[field]:
            if field in data_dict:
                try:
                    data_dict[field] = json.loads(data_dict[field])
                except (ValueError, TypeError):
                    pass
        if isinstance(schema[field], dict):
            if field in data_dict:
                try:
                    data_dict[field] = json.loads(data_dict[field])
                except (ValueError, TypeError):
                    pass

                if not isinstance(data_dict[field], list):
                    data_dict[field] = []


def prepare_resource_by_view(data_dict):
    schema = dcat_schema.dcat_ap_modify_resource_schema()
    _prepare_schema_by_view(data_dict, schema)


def prepare_schema_by_view(data_dict):
    schema = dcat_schema.dcat_ap_modify_package_schema()
    _prepare_schema_by_view(data_dict, schema)


def prepare_org_schema_by_view(data_dict):
    schema = dcat_schema.dcat_ap_modify_organization_schema()
    _prepare_schema_by_view(data_dict, schema)


def dcat_ap_package_for_view(data_dict):
    result = []
    schema = dcat_ap_show_package_schema()
    if TRANSFIELD in schema:
        del schema[TRANSFIELD]

    del schema['translation_meta']
    del schema['original_source']

    for field in schema.keys():
        if field in data_dict and data_dict[field]:
            # Set the human-readable title
            name = field.replace('_', ' ').title()
            # Per default every field is misc
            field_type = 'misc'
            # Check if value is a dict
            if isinstance(data_dict[field], dict):
                field_type = 'dict'
                try:
                    for_view = json.dumps(data_dict[field])
                except ValueError:
                    for_view = data_dict[field]
            # Check if value is a list
            elif isinstance(data_dict[field], list):
                field_type = 'listofdict'
                try:
                    for_view = json.dumps(data_dict[field])
                except ValueError:
                    for_view = data_dict[field]
                for f in data_dict[field]:
                    if not isinstance(f, dict):
                        field_type = 'list'
                        break
            else:
                for_view = data_dict[field]

            include = True
            if field_type == 'list':
                include = False
                for elem in data_dict[field]:
                    if elem != "":
                        include = True

            if include:
                # Resolve RDF resource
                result_dict = {
                    'field': field,
                    'name': name,
                    'value': data_dict[field],
                    'for_view': for_view,
                    'type': field_type
                }

                resolved_dict = _resolve_field(result_dict)
                if resolved_dict is not None:
                    result.append(resolved_dict)
                else:
                    result.append(result_dict)
    return result


def _resolve_field(dic):
    supported_fields = [
        'accrual_periodicity',
        'language',
        'publisher'
    ]
    if dic['field'] not in supported_fields:
        return None

    result = {}
    if dic['type'] == 'dict' and 'resource' in dic['value']:
        try:
            resolved_value = rdf_helpers.resolve_resource(dic['value']['resource'], dic['field'])
            if resolved_value is not None:
                result = {
                    'field': dic['field'],
                    'name': dic['name'],
                    'value': resolved_value,
                    'for_view': resolved_value,
                    'type': 'misc'
                }
            else:
                result = None
        except:
            result = None

    elif dic['type'] == 'listofdict':
        result_value = []
        for elm in dic['value']:
            if 'resource' in elm:
                try:
                    resolved_value = rdf_helpers.resolve_resource(elm['resource'], dic['field'])
                    if resolved_value is not None:
                        result_value.append(resolved_value)
                except:
                    pass

        if len(result_value) > 0:
            result = {
                'field': dic['field'],
                'name': dic['name'],
                'value': result_value,
                'for_view': result_value,
                'type': 'list'
            }
        else:
            result = None
    else:
        result = None
    return result



def dcat_ap_organization_for_view(data_dict):
    result = []
    schema = dcat_ap_show_organization_schema()

    for field in schema.keys():
        if field in data_dict and data_dict[field]:
            name = field.replace('_', ' ').title()
            field_type = 'misc'
            if isinstance(data_dict[field], dict):
                field_type = 'dict'
                try:
                    for_view = json.dumps(data_dict[field])
                except ValueError:
                    for_view = data_dict[field]
            elif isinstance(data_dict[field], list):
                field_type = 'listofdict'
                try:
                    for_view = json.dumps(data_dict[field])
                except ValueError:
                    for_view = data_dict[field]
                for f in data_dict[field]:
                    if not isinstance(f, dict):
                        field_type = 'list'
                        break
            else:
                for_view = data_dict[field]

            result.append({
                'field': field,
                'name': name,
                'value': data_dict[field],
                'for_view': for_view,
                'type': field_type
            })
    return result


def dcat_ap_organization_for_edit(data_dict):
    result = []
    schema = dcat_ap_show_organization_schema()
    for field in schema.keys():
        name = field.replace('_', ' ').title()
        value = None
        if field in data_dict:
            value = data_dict[field]
            if isinstance(data_dict[field], list) or isinstance(data_dict[field], dict):
                try:
                    for_view = json.dumps(data_dict[field])
                except ValueError:
                    for_view = data_dict[field]
            else:
                for_view = data_dict[field]
        else:
            for_view = None

        result.append({
            'field': field,
            'name': name,
            'value': value,
            'for_view': for_view
        })
    return result


def dcat_ap_resource_for_view(data_dict):
    pass


def dcat_ap_package_for_edit(data_dict):
    result = []
    schema = dcat_ap_show_package_schema()

    del schema['translation_meta']
    del schema['translation']

    for field in schema.keys():
        name = field.replace('_', ' ').title()
        value = None
        if field in data_dict:
            value = data_dict[field]
            if isinstance(data_dict[field], list) or isinstance(data_dict[field], dict):
                try:
                    for_view = json.dumps(data_dict[field])
                except ValueError:
                    for_view = data_dict[field]
            else:
                for_view = data_dict[field]
        else:
            for_view = None

        result.append({
            'field': field,
            'name': name,
            'value': value,
            'for_view': for_view
        })
    return result


def dcat_ap_resource_for_edit(data_dict):
    result = []
    schema = dcat_ap_show_resource_schema()
    for field in schema:
        title = field.replace('_', ' ').title()
        value = None
        if field in data_dict:
            value = data_dict[field]
            if isinstance(data_dict[field], list) or isinstance(data_dict[field], dict):
                try:
                    value_for_view = json.dumps(data_dict[field])
                except ValueError:
                    value_for_view = data_dict[field]
            else:
                value_for_view = data_dict[field]
        else:
            value_for_view = None
        result.append(dict(field=field, title=title, value=value, value_for_view=value_for_view))

    return result


def preprocess_data_dict(data_dict):
    """
    :type data_dict: dict
    :param data_dict:
    :return:
    """
    if 'resources' in data_dict:
        resources = data_dict['resources']
        for r in resources:
            if 'url' not in r and 'access_url' in r:
                access_url = r['access_url']
                if isinstance(access_url, list):
                    r['url'] = access_url[0]

            values_in_list = ['conforms_to', 'language']

            for v in values_in_list:
                if v in r:
                    r[v] = convert_list_to_nested_list(r[v])

    # Validate Tag Name, and delete invalid ones
    tags = []
    if 'tags' in data_dict:
        for t in data_dict['tags']:
            try:
                tag_name_validator(t['name'], None)
                tag_length_validator(t['name'], None)
                tags.append(t)
            except tk.Invalid:
                pass
        data_dict['tags'] = tags

    if 'notes' in data_dict and data_dict['notes'] and isinstance(data_dict['notes'], basestring):
        data_dict['notes'] = strip_tags(data_dict['notes'])

    if 'title' in data_dict:
        data_dict['title'] = strip_tags(data_dict['title'])

    if 'modified' not in data_dict or data_dict['modified'] is u'':
        data_dict['modified'] = datetime.datetime.utcnow().isoformat()


def preprocess_organization_dict(data_dict):

    values_in_list = ['language']

    for v in values_in_list:
        if v in data_dict:
            data_dict[v] = convert_list_to_nested_list(data_dict[v])


def process_identifier_field(data_dict):
    field_name = 'identifier'
    if field_name in data_dict:
        ident = json.loads(data_dict[field_name])
        if isinstance(ident, list):
            data_dict[field_name] = ident
            data_dict['source_identifier'] = ident[0]
        else:
            log.debug("Identifier Field is not a list and will be omitted")
            del data_dict[field_name]


