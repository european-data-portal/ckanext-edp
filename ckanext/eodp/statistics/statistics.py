import requests
import json
import datetime
import logging
import pycountry

import sqlalchemy.orm as orm
from ckan.model.meta import CkanCacheExtension, CkanSessionExtension
from ckan.model import extension
import ckan.lib.activity_streams_session_extension as activity

from ckanext.eodp.model import DatasetsPerCategory, DatasetsPerCatalogue, DatasetsPerCountry, \
    DatasetsPerCountryCatalogue, DatasetsPerCountryCategory, DatasetsAssignedToDataCategoriesPerCountry, \
    DatasetsAssignedToDataCategoriesByCountryCatalogue

from ckanext.eodp.statistics import statistics_xls

from ckanext.eodp.statistics import *

log = logging.getLogger(__name__)


spatials = ["AT", "BE", "BG", "CY", "CZ", "DE", "DK", "EE", "ES", "FI", "FR", "GB", "GR", "HR", "HU", "IE", "IT",
            "LT", "LU", "LV", "MT", "NL", "PL", "PT", "RO", "SE", "SI", "SK", "CH", "IS", "LI", "MD", "NO", "RS"
            ]

if CONFIG.get('statistics', 'use_eodp_url') == 'True':
    use_eodp_url = True
else:
    use_eodp_url = False

session = orm.scoped_session(orm.sessionmaker(
                autoflush=False,
                autocommit=False,
                expire_on_commit=False,
                extension=[CkanCacheExtension(),
                           CkanSessionExtension(),
                           extension.PluginSessionExtension(),
                           activity.DatasetActivitySessionExtension()],
            ))


def date_exists_in_table(array, date):
    for i in range(0, len(array)):
        if str(array[i].date.strftime('%Y-%m-%d')) == str(date):
            return True
    return False


def label_exists_in_dict(array, label):
    for i in range(0, len(array)):
        if array[i].label.replace(" ", "").replace("-", "").replace(".", "").lower() == \
                label.replace(" ", "").replace("-", "").replace(".", "").lower():
            return array[i].label
    return label


def datasets_per_category():
    result = []
    if use_eodp_url:
        r = requests.get("http://www.europeandataportal.eu/data/api/3/action/group_list?all_fields=true")
    else:
        r = requests.get(SITE_URL + "/api/3/action/group_list?all_fields=true")

    group_list = json.loads(r.text)['result']

    print('1:###### Number of data sets per category')

    datasets_list = DatasetsPerCategory.get_data()

    current_date = datetime.datetime.today().strftime('%Y-%m-%d')

    if not date_exists_in_table(datasets_list, current_date):
        for elem in group_list:
            datasets_per_category = DatasetsPerCategory()
            datasets_per_category.date = current_date
            datasets_per_category.label = label_exists_in_dict(datasets_list, elem['title'])
            datasets_per_category.count = elem['package_count']

            datasets_per_category.Session = session
            datasets_per_category.save()

            print "datasets ", datasets_per_category

            result.append(datasets_per_category)
        return result

    else:
        print "datasets from current_date: " + current_date + " are already existing"


def datasets_per_catalogue():
    result = []
    if use_eodp_url:
        r = requests.get("http://www.europeandataportal.eu/data/api/3/action/organization_list?all_fields=true")
    else:
        r = requests.get(SITE_URL + "/api/3/action/organization_list?all_fields=true")

    organization_list = json.loads(r.text)['result']

    print('2:###### Number of datasets per catalogue')

    datasets_list = DatasetsPerCatalogue.get_data()

    current_date = datetime.datetime.today().strftime('%Y-%m-%d')

    if not date_exists_in_table(datasets_list, current_date):
        for elem in organization_list:
            if use_eodp_url:
                r = requests.get('http://www.europeandataportal.eu/data/api/3/action/package_search?q=organization:' + elem['name'])
            else:
                r = requests.get(SITE_URL + '/api/3/action/package_search?q=organization:' + elem['name'])

            catalogue = json.loads(r.text)['result']
            count = catalogue['count']

            datasets_per_catalogue = DatasetsPerCatalogue()
            datasets_per_catalogue.date = current_date
            datasets_per_catalogue.label = label_exists_in_dict(datasets_list, elem['title'])
            datasets_per_catalogue.count = count

            datasets_per_catalogue.Session = session
            datasets_per_catalogue.save()

            print "datasets ", datasets_per_catalogue

            result.append(datasets_per_catalogue)
        return result
    else:
        print "datasets from current_date: " + current_date + " are already existing"


def datasets_per_country_catalogue():
    result = []
    print('3:###### Number of datasets per country and catalogue')

    datasets_list = DatasetsPerCountryCatalogue.get_data()

    current_date = datetime.datetime.today().strftime('%Y-%m-%d')

    if not date_exists_in_table(datasets_list, current_date):
        for s in spatials:
            name = pycountry.countries.get(alpha2=s).name
            # catalogues = get.organization_list({}, {'spatial': s, 'all_fields': True})
            if use_eodp_url:
                catalogues = requests.get('http://www.europeandataportal.eu/data/api/3/action/organization_list?spatial=' + s + '&all_fields=true')
            else:
                catalogues = requests.get(SITE_URL + '/api/3/action/organization_list?spatial=' + s + '&all_fields=true')
            catalogues = json.loads(catalogues.text)['result']

            # print('### Country: ' + s + ' - ' + name)

            for catalogue in catalogues:
                if use_eodp_url:
                    r = requests.get(
                        'http://www.europeandataportal.eu/data/api/3/action/package_search?q=organization:' + catalogue[
                            'name'])
                else:
                    r = requests.get(SITE_URL + '/api/3/action/package_search?q=organization:' + catalogue['name'])

                catalogue_r = json.loads(r.text)['result']
                count = catalogue_r['count']

                datasets_per_country_catalogue = DatasetsPerCountryCatalogue()
                datasets_per_country_catalogue.date = current_date
                datasets_per_country_catalogue.label = label_exists_in_dict(datasets_list, s + ': ' + catalogue['title'])
                datasets_per_country_catalogue.count = count

                datasets_per_country_catalogue.Session = session
                datasets_per_country_catalogue.save()

                print "datasets ", datasets_per_country_catalogue

                result.append(datasets_per_country_catalogue)
        return result
    else:
        print "datasets from current_date: " + current_date + " are already existing"


def datasets_per_country():
    result = []
    print('4:###### Number of datasets per country')

    datasets_list = DatasetsPerCountry.get_data()

    current_date = datetime.datetime.today().strftime('%Y-%m-%d')

    if not date_exists_in_table(datasets_list, current_date):
        for s in spatials:
            name = pycountry.countries.get(alpha2=s).name
            # catalogues = get.organization_list({}, {'spatial': s, 'all_fields': True})
            if use_eodp_url:
                catalogues = requests.get('http://www.europeandataportal.eu/data/api/3/action/organization_list?spatial=' + s + '&all_fields=true')
            else:
                catalogues = requests.get(SITE_URL + '/api/3/action/organization_list?spatial=' + s + '&all_fields=true')
            catalogues = json.loads(catalogues.text)['result']

            count = 0
            for catalogue in catalogues:
                if use_eodp_url:
                    r = requests.get(
                        'http://www.europeandataportal.eu/data/api/3/action/package_search?q=organization:' + catalogue['name'])
                else:
                    r = requests.get(SITE_URL + '/api/3/action/package_search?q=organization:' + catalogue['name'])

                catalogue_r = json.loads(r.text)['result']
                count += catalogue_r['count']

            datasets_per_country = DatasetsPerCountry()
            datasets_per_country.date = current_date
            datasets_per_country.label = label_exists_in_dict(datasets_list, s + ':' + name)
            datasets_per_country.count = count

            datasets_per_country.Session = session
            datasets_per_country.save()

            print "datasets ", datasets_per_country

            result.append(datasets_per_country)
        return result
    else:
        print "datasets from current_date: " + current_date + " are already existing"


def datasets_per_country_category():
    result = []
    if use_eodp_url:
        r = requests.get("http://www.europeandataportal.eu/data/api/3/action/group_list?all_fields=true")
    else:
        r = requests.get(SITE_URL + "/api/3/action/group_list?all_fields=true")

    domains = json.loads(r.text)['result']

    print('5:###### Number of datasets per country and category')

    datasets_list = DatasetsPerCountryCategory.get_data()

    current_date = datetime.datetime.today().strftime('%Y-%m-%d')

    if not date_exists_in_table(datasets_list, current_date):
        for s in spatials:
            name = pycountry.countries.get(alpha2=s).name
            # catalogues = get.organization_list({}, {'spatial': s, 'all_fields': True})
            if use_eodp_url:
                catalogues = requests.get(
                    'http://www.europeandataportal.eu/data/api/3/action/organization_list?spatial=' + s + '&all_fields=true')
            else:
                catalogues = requests.get(SITE_URL + '/api/3/action/organization_list?spatial=' + s + '&all_fields=true')
            catalogues = json.loads(catalogues.text)['result']

            for domain in domains:
                count = 0
                for catalogue in catalogues:
                    if use_eodp_url:
                        r = requests.get(
                            'http://www.europeandataportal.eu/data/api/3/action/package_search?q=groups:' + domain[
                                'name'] + '+organization:' + catalogue['name'])
                    else:
                        r = requests.get(
                            SITE_URL + 'api/3/action/package_search?q=groups:' + domain['name'] +
                            '+organization:' + catalogue['name'])

                    r = json.loads(r.text)['result']
                    count += r['count']

                datasets_per_country_category = DatasetsPerCountryCategory()
                datasets_per_country_category.date = current_date
                datasets_per_country_category.label = label_exists_in_dict(datasets_list, s + ':' + domain['title'])
                datasets_per_country_category.count = count

                datasets_per_country_category.Session = session
                datasets_per_country_category.save()

                print "datasets ", datasets_per_country_category

                result.append(datasets_per_country_category)
        return result
    else:
        print "datasets from current_date: " + current_date + " are already existing"


def datasets_assigned_to_data_categories_per_country():
    result = []
    if use_eodp_url:
        r = requests.get("http://www.europeandataportal.eu/data/api/3/action/group_list?all_fields=true")
    else:
        r = requests.get(SITE_URL + "/api/3/action/group_list?all_fields=true")

    domains = json.loads(r.text)['result']

    print('6:###### Number of assigned datasets to data categories per country')

    datasets_list = DatasetsAssignedToDataCategoriesPerCountry.get_data()

    current_date = datetime.datetime.today().strftime('%Y-%m-%d')

    if not date_exists_in_table(datasets_list, current_date):
        for s in spatials:
            name = pycountry.countries.get(alpha2=s).name
            # catalogues = get.organization_list({}, {'spatial': s, 'all_fields': True})
            if use_eodp_url:
                catalogues = requests.get('http://www.europeandataportal.eu/data/api/3/action/organization_list?spatial=' + s + '&all_fields=true')
            else:
                catalogues = requests.get(SITE_URL + '/api/action/organization_list?spatial=' + s + '&all_fields=true')

            catalogues = json.loads(catalogues.text)['result']

            total_country_datasets_count = 0
            assigned_count = 0

            for catalogue in catalogues:
                if use_eodp_url:
                    r = requests.get(
                       'http://www.europeandataportal.eu/data/api/3/action/package_search?q=organization:' + catalogue['name'])
                else:
                    r = requests.get(
                        SITE_URL + '/api/3/action/package_search?q=organization:' + catalogue['name'])

                r = json.loads(r.text)['result']
                total_country_datasets_count += r['count']

                for domain in domains:
                    if use_eodp_url:
                        r = requests.get(
                            'http://www.europeandataportal.eu/data/api/3/action/package_search?q=groups:' + domain[
                                'name'] + '+organization:' + catalogue['name'])

                    else:
                        r = requests.get(SITE_URL + '/api/3/action/package_search?q=groups:' + domain['name'] +
                                         '+organization:' + catalogue['name'])

                    r = json.loads(r.text)['result']

                    assigned_count += r['count']

            datasets_assigned_to_data_categories_per_country = DatasetsAssignedToDataCategoriesPerCountry()
            datasets_assigned_to_data_categories_per_country.date = current_date
            datasets_assigned_to_data_categories_per_country.label = label_exists_in_dict(datasets_list, s + ': ' + name)
            datasets_assigned_to_data_categories_per_country.total_count = total_country_datasets_count
            datasets_assigned_to_data_categories_per_country.assigned_count = assigned_count

            datasets_assigned_to_data_categories_per_country.Session = session
            datasets_assigned_to_data_categories_per_country.save()
            print "datasets ", datasets_assigned_to_data_categories_per_country

            result.append(datasets_assigned_to_data_categories_per_country)
        return result
    else:
        print "datasets from current_date: " + current_date + " are already existing"


def datasets_assigned_to_categories_by_country_and_catalogue():
    result = []
    if use_eodp_url:
        r = requests.get("http://www.europeandataportal.eu/data/api/3/action/group_list?all_fields=true")
    else:
        r = requests.get(SITE_URL + "/api/3/action/group_list?all_fields=true")

    domains = json.loads(r.text)['result']

    print('7:###### Number of datasets assigned to categories by country and by catalogue')

    datasets_list = DatasetsAssignedToDataCategoriesByCountryCatalogue.get_data()

    current_date = datetime.datetime.today().strftime('%Y-%m-%d')

    if not date_exists_in_table(datasets_list, current_date):
        for s in spatials:
            # catalogues = get.organization_list({}, {'spatial': s, 'all_fields': True})
            if use_eodp_url:
                catalogues = requests.get(
                    'http://www.europeandataportal.eu/data/api/3/action/organization_list?spatial=' + s + '&all_fields=true')
            else:
                catalogues = requests.get(SITE_URL + '/api/3/action/organization_list?spatial=' + s + '&all_fields=true')
            catalogues = json.loads(catalogues.text)['result']

            for catalogue in catalogues:
                if use_eodp_url:
                    r = requests.get(
                       'http://www.europeandataportal.eu/data/api/3/action/package_search?q=organization:' + catalogue['name'])
                else:
                    r = requests.get(SITE_URL + '/api/3/action/package_search?q=organization:' + catalogue['name'])

                r = json.loads(r.text)['result']

                total_catalogue_datasets_count = r['count']
                assigned_count = 0

                for domain in domains:
                    if use_eodp_url:
                        r = requests.get(
                            'http://www.europeandataportal.eu/data/api/3/action/package_search?q=groups:' + domain[
                                'name'] + '+organization:' + catalogue['name'])
                    else:
                        r = requests.get(SITE_URL + '/api/3/action/package_search?q=groups:' + domain['name'] +
                                         '+organization:' + catalogue['name'])

                    r = json.loads(r.text)['result']

                    assigned_count += r['count']

                datasets_assigned_to_data_categories_per_country_catalogue = DatasetsAssignedToDataCategoriesByCountryCatalogue()
                datasets_assigned_to_data_categories_per_country_catalogue.date = current_date
                datasets_assigned_to_data_categories_per_country_catalogue.label = label_exists_in_dict(datasets_list, s + ': ' + catalogue['title'])
                datasets_assigned_to_data_categories_per_country_catalogue.total_count = total_catalogue_datasets_count
                datasets_assigned_to_data_categories_per_country_catalogue.assigned_count = assigned_count

                datasets_assigned_to_data_categories_per_country_catalogue.Session = session
                datasets_assigned_to_data_categories_per_country_catalogue.save()

                print "datasets ", datasets_assigned_to_data_categories_per_country_catalogue

                result.append(datasets_assigned_to_data_categories_per_country_catalogue)
        return result
    else:
        print "datasets from current_date: " + current_date + " are already existing"


def call_all_functions():
    datasets_list = DatasetsPerCategory.get_data()

    current_date = datetime.datetime.today().strftime('%Y-%m-%d')

    if not date_exists_in_table(datasets_list, current_date):
        datasets_per_category()
        datasets_per_catalogue()
        datasets_per_country_catalogue()
        datasets_per_country()
        datasets_per_country_category()
        datasets_assigned_to_data_categories_per_country()
        datasets_assigned_to_categories_by_country_and_catalogue()

        print "successfully posted new data to tables"
    else:
        print "datasets from current_date: " + current_date + " are already existing"


def call_all_functions_with_xls():
    datasets_list = DatasetsPerCategory.get_data()

    current_date = datetime.datetime.today().strftime('%Y-%m-%d')

    if not date_exists_in_table(datasets_list, current_date):
        call_all_functions()
        statistics_xls.call_all_functions()
    else:
        print "datasets from current_date: " + current_date + " are already existing"


def delete_tables():
    DatasetsPerCategory.delete_table()
    DatasetsPerCatalogue.delete_table()
    DatasetsPerCountryCategory.delete_table()
    DatasetsPerCountry.delete_table()
    DatasetsPerCountryCatalogue.delete_table()
    DatasetsAssignedToDataCategoriesPerCountry.delete_table()
    DatasetsAssignedToDataCategoriesByCountryCatalogue.delete_table()

    statistics_xls.delete_xls_file()
