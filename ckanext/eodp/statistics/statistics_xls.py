import requests
import openpyxl
from openpyxl.styles import Alignment, Border, Font, PatternFill, Side
from openpyxl.formatting.rule import ColorScaleRule
import os.path
import datetime
from ckanext.eodp.model import DatasetsPerCategory, DatasetsPerCatalogue, DatasetsPerCountry, \
    DatasetsPerCountryCatalogue, DatasetsPerCountryCategory, DatasetsAssignedToDataCategoriesPerCountry, \
    DatasetsAssignedToDataCategoriesByCountryCatalogue

from ckanext.eodp.statistics import *


def create_xls_file():
    wb = openpyxl.Workbook()
    wb.encoding = "utf-8"
    wb.worksheets[0].title = "DS per category"
    wb.worksheets[0].cell(row=1, column=1).value = "Number of datasets per category"
    wb.create_sheet("DS per catalogue")
    wb.worksheets[1].cell(row=1, column=1).value = "Number of datasets per catalogue"
    wb.create_sheet("DS per country and catalogue")
    wb.worksheets[2].cell(row=1, column=1).value = "Number of datasets per country and catalogue"
    wb.create_sheet("DS per country")
    wb.worksheets[3].cell(row=1, column=1).value = "Number of datasets per country"
    wb.create_sheet("DS per country and category")
    wb.worksheets[4].cell(row=1, column=1).value = "Number of datasets per country and category"
    wb.create_sheet("DS assigned to data cat")
    wb.worksheets[5].cell(row=1, column=1).value = "Number of assigned datasets to data cat per country"
    wb.create_sheet("DS assigned country and cat")
    wb.worksheets[6].cell(row=1, column=1).value = "Number of assigned datasets to data cat per country and " \
                                                   "catalogue"
    wb.save(XLS_FILE_PATH)


def add_latest_to_xls():
    if not os.path.isfile(XLS_FILE_PATH):
        create_xls_file()

    # open the xlsx file
    workbook = openpyxl.load_workbook(filename=XLS_FILE_PATH)
    workbook.encoding = 'utf-8'

    # create styling variables
    font_bold = Font(bold=True, name='Calibri')
    font_thin = Font(bold=False, name='Calibri')

    bd_thin = Side(style="thin", color="000000")
    bd_thick = Side(style="thick", color="000000")

    border_thin = Border(left=bd_thin, top=bd_thin, right=bd_thin, bottom=bd_thin)
    border_thick = Border(left=bd_thick, top=bd_thick, right=bd_thick, bottom=bd_thick)

    blue_fill = PatternFill(start_color="b0d6f2", end_color="b0d6f2", fill_type="solid")

    color_scale_rule = ColorScaleRule(start_type='percentile', start_value=10, start_color='f06464',
                                      mid_type='percentile', mid_value=50, mid_color='f0e364',
                                      end_type='percentile', end_value=90, end_color='85f064')

    def get_max_column(worksheet):
        current_column = 2
        while worksheet.cell(row=3, column=current_column).value is not None:
            current_column += 1
        return current_column - 1

    def get_max_row(worksheet, start_row):
        current_row = start_row
        while worksheet.cell(row=current_row, column=1).value is not None:
            current_row += 1
        return current_row - start_row - 1

    def get_all_entries_sorted(datasets):
        """
        sorts all datasets by date and creates array with entry dictionaries
        :param datasets:
        :return: array with all entries
        """
        all_entries = []
        datasets = sorted(datasets, key=lambda key: key.date, reverse=True)

        for i in range(0, len(datasets)):
            if hasattr(datasets[i], 'total_count'):
                all_entries.append(
                    {"date": datasets[i].date, "label": datasets[i].label,
                     "total_count": datasets[i].total_count,
                     "assigned_count": datasets[i].assigned_count})

            else:
                all_entries.append({"date": datasets[i].date, "label": datasets[i].label,
                                       "count": datasets[i].count})

        return all_entries

    def get_latest_entries(datasets):
        """
        sorts all datasets by date and creates array with entry dictionaries with the newest datasets
        :param datasets:
        :return: array with newest entries
        """

        latest_entries = []

        datasets = sorted(datasets, key=lambda key: key.date, reverse=True)

        current_date = datasets[0].date
        latest_datasets = []
        for k in range(0, len(datasets)):
            if current_date != datasets[k].date:
                break
            else:
                latest_datasets.append(datasets[k])

        latest_datasets = sorted(latest_datasets, key=lambda key: key.label)

        for l in range(0, len(latest_datasets)):
            if hasattr(latest_datasets[l], 'total_count'):
                latest_entries.append(
                    {"date": latest_datasets[l].date, "label": latest_datasets[l].label,
                     "total_count": latest_datasets[l].total_count,
                     "assigned_count": latest_datasets[l].assigned_count})

            else:
                latest_entries.append({"date": latest_datasets[l].date, "label": latest_datasets[l].label,
                                       "count": latest_datasets[l].count})

        return latest_entries

    def get_column_size_max(dataset_list):
        """
        returns the maximum length of the labels from the given dataset_list
        :param dataset_list:
        :return:
        """
        length = 0
        for dataset in dataset_list:
            if length < len(dataset['label']):
                length = len(dataset['label'])
        return length

    def get_entry_from_datasets_list(datasets_list, label, date, first_sheets):
        """
        get entry from all_entries for specific label and date
        :param datasets_list: array of all entries from one table
        :param label: label
        :param date: date
        :param first_sheets: true if values should be used for first sheets
        :return: entry or none if no entry is existing with the specific label and date
        """
        for i in range(0, len(datasets_list)):
            for j in range(0, len(datasets_list[i])):
                if datasets_list[i][j]['label'].replace(" ", "").replace("-", "").replace(".", "").lower() == \
                        label.replace(" ", "").replace("-", "").replace(".", "").lower() \
                        and str(datasets_list[i][j]['date']) == date:
                    if first_sheets:
                        if 'count' in datasets_list[i][j]:
                            return datasets_list[i][j]
                    else:
                        if 'total_count' in datasets_list[i][j]:
                            return datasets_list[i][j]
        return None

    def prepare_style_first_sheets(index, row, column, column_letter):
        """
        handles the styling of the xls file e.g. the border, color and font-style of the date rorw
        :param index: current index from the datasets_list
        :param row: current row
        :param column: current column
        :param column_letter: the letter from the current column
        """
        workbook.worksheets[index].cell(row=row, column=column).value = str(datasets_list_latest[index][0]['date'])
        workbook.worksheets[index].cell(row=row, column=column).font = font_bold
        workbook.worksheets[index].cell(row=row, column=column).border = border_thick
        workbook.worksheets[index].column_dimensions[column_letter].width = len(str(datasets_list_latest[index][0]['date']))
        workbook.worksheets[index].cell(row=row, column=column).fill = blue_fill

        workbook.worksheets[index].cell(row=row, column=column + 1).value = "Avg increase in %"
        workbook.worksheets[index].cell(row=row, column=column + 1).font = font_bold
        workbook.worksheets[index].cell(row=row, column=column + 1).border = border_thick
        workbook.worksheets[index].column_dimensions[workbook.worksheets[index].cell(row=row, column=column + 1)
            .column].width = len("Avg increase in %")
        workbook.worksheets[index].cell(row=row, column=column + 1).fill = blue_fill

        workbook.worksheets[index].cell(row=row, column=column - 1).font = font_bold
        workbook.worksheets[index].cell(row=row, column=column - 1).border = border_thick
        workbook.worksheets[index].cell(row=row, column=column - 1).fill = blue_fill

    def prepare_style_next_sheets(index, row, column):
        """
        handles the styling of the xls file e.g. the border, color and font-style of the date rorw
        :param index: current index from the datasets_list
        :param row: current row
        :param column: current column
        """
        column_letter_1 = workbook.worksheets[index].cell(row=row, column=column + 1).column
        column_letter_2 = workbook.worksheets[index].cell(row=row, column=column + 2).column
        column_letter_3 = workbook.worksheets[index].cell(row=row, column=column + 3).column

        workbook.worksheets[index].merge_cells(start_row=row, start_column=1, end_row=row + 1, end_column=1)
        workbook.worksheets[index].merge_cells(start_row=row, start_column=column + 1, end_row=row,
                                           end_column=column + 3)

        assigned_ds_string = 'assigned #DS'

        workbook.worksheets[index].cell(row=row, column=column).font = font_bold
        workbook.worksheets[index].cell(row=row, column=column).border = border_thick
        workbook.worksheets[index].cell(row=row, column=column).fill = blue_fill

        workbook.worksheets[index].cell(row=row, column=column + 1).value = str(datasets_list_latest[index][0]['date'])
        workbook.worksheets[index].cell(row=row, column=column + 1).font = font_bold
        workbook.worksheets[index].cell(row=row, column=column + 1).alignment = Alignment(horizontal='center')
        workbook.worksheets[index].cell(row=row, column=column + 1).border = border_thick
        workbook.worksheets[index].cell(row=row, column=column + 1).fill = blue_fill

        workbook.worksheets[index].cell(row=row + 1, column=column + 1).value = 'Total #DS'
        workbook.worksheets[index].cell(row=row + 1, column=column + 1).font = font_bold
        workbook.worksheets[index].cell(row=row + 1, column=column + 1).border = border_thick
        workbook.worksheets[index].cell(row=row + 1, column=column + 1).fill = blue_fill
        workbook.worksheets[index].column_dimensions[column_letter_1].width = len(assigned_ds_string)

        workbook.worksheets[index].cell(row=row + 1, column=column + 2).value = assigned_ds_string
        workbook.worksheets[index].cell(row=row + 1, column=column + 2).font = font_bold
        workbook.worksheets[index].cell(row=row + 1, column=column + 2).border = border_thick
        workbook.worksheets[index].cell(row=row + 1, column=column + 2).fill = blue_fill
        workbook.worksheets[index].column_dimensions[column_letter_2].width = len(assigned_ds_string)

        workbook.worksheets[index].cell(row=row + 1, column=column + 3).value = 'In %'
        workbook.worksheets[index].cell(row=row + 1, column=column + 3).font = font_bold
        workbook.worksheets[index].cell(row=row + 1, column=column + 3).border = border_thick
        workbook.worksheets[index].cell(row=row + 1, column=column + 3).fill = blue_fill
        workbook.worksheets[index].column_dimensions[column_letter_3].width = len(assigned_ds_string)

    def add_values_first_sheets(index, row, column, max_row, max_column, write_label, column_letter, column_size):
        """
        adds values to the category, catalogues, country_catalogue, country, country_category sheets
        :param index: index of the current worksheet
        :param row: current row
        :param column: current column
        :param max_row:
        :param max_column:
        :param write_label: bool if labels should be overwritten or not
        :param column_letter: letter of the current column
        :param column_size: the size that the columns will have
        :return: row
        """
        for j in range(0, max_row):
            if write_label is True:
                # add label to first column and specific row
                workbook.worksheets[index].cell(row=row, column=1).value = datasets_list_latest[index][j]['label']
                workbook.worksheets[index].cell(row=row, column=1).border = border_thin
                workbook.worksheets[index].cell(row=row, column=1).font = font_thin
                workbook.worksheets[index].column_dimensions['A'].width = column_size
                column = 2

            for c in range(column, max_column + 1):
                entry_value = get_entry_from_datasets_list(datasets_list_all,
                                                         workbook.worksheets[index].cell(row=row, column=1).value,
                                                         workbook.worksheets[index].cell(row=1, column=c).value, True)
                if workbook.worksheets[index].cell(row=row, column=1).value is not None:
                    if entry_value is not None:
                        workbook.worksheets[index].cell(row=row, column=c).value = entry_value['count']
                    else:
                        workbook.worksheets[index].cell(row=row, column=c).value = 0
                else:
                    workbook.worksheets[index].cell(row=row, column=c).value = 0

                # reset styling of column
                workbook.worksheets[index].cell(row=row, column=c).number_format = "0"
                workbook.worksheets[index].cell(row=row, column=c).border = border_thin
                workbook.worksheets[index].cell(row=row, column=c).font = font_thin

            # average calculation from first column to last in percentage
            workbook.worksheets[index].cell(row=row, column=max_column + 1).value = "=IF(" + "B" + \
                                                                                str(row) + "=0,0,(AVERAGE(B" + \
                                                                                str(row) + \
                                                                                ":" + column_letter + \
                                                                                str(row) + ")-B" + \
                                                                                str(row) + ")/B" + \
                                                                                str(row) + ")"
            workbook.worksheets[index].cell(row=row, column=max_column + 1).number_format = "0.00%"
            workbook.worksheets[index].cell(row=row, column=max_column + 1).font = font_bold
            workbook.worksheets[index].cell(row=row, column=max_column + 1).border = border_thick

            row += 1

        return row

    def add_values_next_sheets(index, row, column, max_row, loop_limit, write_label, column_size):
        """
        adds values to the assigned_to_data_categories_per_country, assigned_to_categories_by_country_and_catalogue sheets
        :param index: index of the current worksheet
        :param row: current row
        :param column: current column
        :param max_row:
        :param loop_limit: the max value for the while loop
        :param write_label: bool if labels should be overwritten or not
        :param column_size: the size that the columns will have
        :return: row
        """
        for j in range(0, max_row):
            if write_label:
                # add label to first column and specific row
                workbook.worksheets[index].cell(row=row, column=1).value = datasets_list_latest[index][j]['label']
                workbook.worksheets[index].cell(row=row, column=1).border = border_thin
                workbook.worksheets[index].cell(row=row, column=1).font = font_thin
                workbook.worksheets[index].column_dimensions['A'].width = column_size
                column = 2

            c = column
            while c < loop_limit:
                entry_values = get_entry_from_datasets_list(datasets_list_all,
                                                          workbook.worksheets[index].cell(row=row, column=1).value,
                                                          workbook.worksheets[index].cell(row=1, column=c).value, False)

                if workbook.worksheets[index].cell(row=row, column=1).value is not None:
                    if entry_values is not None:
                        workbook.worksheets[index].cell(row=row, column=c).value = entry_values['total_count']
                        workbook.worksheets[index].cell(row=row, column=c + 1).value = entry_values['assigned_count']
                    else:
                        workbook.worksheets[index].cell(row=row, column=c).value = 0
                        workbook.worksheets[index].cell(row=row, column=c + 1).value = 0
                else:
                    workbook.worksheets[index].cell(row=row, column=c).value = 0
                    workbook.worksheets[index].cell(row=row, column=c + 1).value = 0

                # reset styling of the next columns
                workbook.worksheets[index].cell(row=row, column=c).number_format = "0"
                workbook.worksheets[index].cell(row=row, column=c).border = border_thin
                workbook.worksheets[index].cell(row=row, column=c).font = font_thin

                workbook.worksheets[index].cell(row=row, column=c + 1).number_format = "0"
                workbook.worksheets[index].cell(row=row, column=c + 1).border = border_thin
                workbook.worksheets[index].cell(row=row, column=c + 1).font = font_thin

                # column letters neaded for excel function
                column_letter_1 = workbook.worksheets[index].cell(row=row, column=c).column
                column_letter_2 = workbook.worksheets[index].cell(row=row, column=c + 1).column

                # calculation of how many datasets are assigned to catalogues or categories in percentage
                workbook.worksheets[index].cell(row=row, column=c + 2).value = "=IF(" + column_letter_2 + \
                                                                           str(row) + "=0,0," + column_letter_2 + \
                                                                           str(row) + "/" + \
                                                                           column_letter_1 + str(row) + ")"
                workbook.worksheets[index].cell(row=row, column=c + 2).number_format = "0.00%"
                workbook.worksheets[index].cell(row=row, column=c + 2).border = border_thick
                workbook.worksheets[index].cell(row=row, column=c + 2).font = font_bold

                # increase by 3 because of Total #DS, Assigned #DS, In %
                c += 3
            row += 1

        return row

    def add_functions_first_sheets(index, max_column, row, add_total_datasets, column_letter):
        """
        adds the excel functions, e.g. the sum of the datasets, to the category, catalogues, country_catalogue, country,
        country_category sheets
        :param index: index of the current worksheet
        :param max_column:
        :param row:
        :param add_total_datasets: bool if '###### Total #datasets' row needs to be renewed or not
        :param column_letter:
        :return:
        """
        for c in range(2, max_column + 1):
            column_letter = workbook.worksheets[index].cell(row=row, column=c).column
            workbook.worksheets[index].cell(row=row, column=c).value = "=SUM(" + column_letter + str(2) + ":" + \
                                                                   column_letter + str(row - 1) + ")"
            workbook.worksheets[index].cell(row=row, column=c).font = font_bold
            workbook.worksheets[index].cell(row=row, column=c).border = border_thick

        if add_total_datasets:
            workbook.worksheets[index].cell(row=row, column=1).value = "###### Total #datasets"
            workbook.worksheets[index].cell(row=row, column=1).font = font_bold
            workbook.worksheets[index].cell(row=row, column=1).border = border_thick

        workbook.worksheets[index].cell(row=row, column=max_column).number_format = "0"

        workbook.worksheets[index].cell(row=row, column=max_column + 1).value = "=IF(" + "B" + \
                                                                            str(row) + "=0,0,(AVERAGE(B" + \
                                                                            str(row) + \
                                                                            ":" + column_letter + \
                                                                            str(row) + ")-B" + \
                                                                            str(row) + ")/B" + \
                                                                            str(row) + ")"
        workbook.worksheets[index].cell(row=row, column=max_column + 1).number_format = "0.00%"
        workbook.worksheets[index].cell(row=row, column=max_column + 1).font = font_bold
        workbook.worksheets[index].cell(row=row, column=max_column + 1).border = border_thick

    def add_functions_next_sheets(index, row, loop_limit, add_total_datasets):
        """
        adds the excel functions, e.g. the sum of the datasets, to the assigned_to_data_categories_per_country,
        assigned_to_categories_by_country_and_catalogue sheets
        :param index: index of the current worksheet
        :param row:

        :param loop_limit: the max value for the while loop
        :param add_total_datasets: bool if '###### Total #datasets' row needs to be renewed or not
        :return:
        """
        column = 2
        while column < loop_limit:
            column_letter_1 = workbook.worksheets[index].cell(row=row, column=column).column
            column_letter_2 = workbook.worksheets[index].cell(row=row, column=column + 1).column
            column_letter_3 = workbook.worksheets[index].cell(row=row, column=column + 2).column

            workbook.worksheets[index].cell(row=row, column=column).value = "=SUM(" + column_letter_1 + str(
                3) + ":" + column_letter_1 + str(row - 1) + ")"
            workbook.worksheets[index].cell(row=row, column=column).font = font_bold
            workbook.worksheets[index].cell(row=row, column=column).border = border_thick

            workbook.worksheets[index].cell(row=row, column=column + 1).value = "=SUM(" + column_letter_2 + str(
                3) + ":" + column_letter_2 + str(row - 1) + ")"
            workbook.worksheets[index].cell(row=row, column=column + 1).font = font_bold
            workbook.worksheets[index].cell(row=row, column=column + 1).border = border_thick

            if add_total_datasets:
                workbook.worksheets[index].cell(row=row, column=1).value = "###### Total #datasets"
                workbook.worksheets[index].cell(row=row, column=1).font = font_bold
                workbook.worksheets[index].cell(row=row, column=1).border = border_thick

            workbook.worksheets[index].cell(row=row, column=column + 2).value = "=IF(" + column_letter_2 + \
                                                                       str(row) + "=0,0," + column_letter_2 + \
                                                                       str(row) + "/" + \
                                                                       column_letter_1 + str(row) + ")"
            workbook.worksheets[index].cell(row=row, column=column + 2).number_format = "0.00%"
            workbook.worksheets[index].cell(row=row, column=column + 2).border = border_thick
            workbook.worksheets[index].cell(row=row, column=column + 2).font = font_bold

            workbook.worksheets[index].conditional_formatting.add(column_letter_3 + str(
                3) + ":" + column_letter_3 + str(row - 1), color_scale_rule)

            column += 3

    def fill_first_sheets():
        """
        iterates over the worksheets and calls the different functions to fill the category, catalogues,
        country_catalogue, country, country_category sheets
        :return:
        """
        for i in range(0, len(workbook.worksheets) - 2):
            max_column = get_max_column(workbook.worksheets[i])
            if max_column == 1:
                max_column += 1
            column = max_column

            write_label = False

            row = 1
            column_letter = workbook.worksheets[i].cell(row=row, column=column).column

            prepare_style_first_sheets(i, row, column, column_letter)

            column_size = get_column_size_max(datasets_list_latest[i])

            max_row = get_max_row(workbook.worksheets[i], 2)

            if len(datasets_list_latest[i]) > max_row:
                write_label = True
                max_row = len(datasets_list_latest[i])

            row = add_values_first_sheets(i, row+1, column, max_row, max_column, write_label, column_letter, column_size)

            add_functions_first_sheets(i, max_column, row, write_label, column_letter)

    def fill_next_sheets():
        """
        iterates over the worksheets and calls the different functions to fill the
        assigned_to_data_categories_per_country, assigned_to_categories_by_country_and_catalogue sheets
        :return:
        """
        for i in range(len(workbook.worksheets) - 2, len(workbook.worksheets)):
            row = 1
            max_column = get_max_column(workbook.worksheets[i])
            column = max_column
            write_label = False

            column_size = get_column_size_max(datasets_list_latest[i])

            prepare_style_next_sheets(i, row, column)

            max_row = get_max_row(workbook.worksheets[i], 3)

            if len(datasets_list_latest[i]) > max_row:
                max_row = len(datasets_list_latest[i])
                write_label = True

            row += 2
            column += 1

            loop_limit = max_column + 2

            row = add_values_next_sheets(i, row, column, max_row, loop_limit, write_label, column_size)

            add_functions_next_sheets(i, row, loop_limit, write_label)

    datasets_list_latest = [get_latest_entries(DatasetsPerCategory.get_data()),
                            get_latest_entries(DatasetsPerCatalogue.get_data()),
                            get_latest_entries(DatasetsPerCountryCatalogue.get_data()),
                            get_latest_entries(DatasetsPerCountry.get_data()),
                            get_latest_entries(DatasetsPerCountryCategory.get_data()),
                            get_latest_entries(DatasetsAssignedToDataCategoriesPerCountry.get_data()),
                            get_latest_entries(DatasetsAssignedToDataCategoriesByCountryCatalogue.get_data())
                            ]

    datasets_list_all = [get_all_entries_sorted(DatasetsPerCategory.get_data()),
                         get_all_entries_sorted(DatasetsPerCatalogue.get_data()),
                         get_all_entries_sorted(DatasetsPerCountryCatalogue.get_data()),
                         get_all_entries_sorted(DatasetsPerCountry.get_data()),
                         get_all_entries_sorted(DatasetsPerCountryCategory.get_data()),
                         get_all_entries_sorted(DatasetsAssignedToDataCategoriesPerCountry.get_data()),
                         get_all_entries_sorted(DatasetsAssignedToDataCategoriesByCountryCatalogue.get_data())
                         ]

    fill_first_sheets()
    fill_next_sheets()

    save_xlsx_file(workbook)


def save_xlsx_file(workbook):
    workbook.save(XLS_FILE_PATH)
    workbook.save(XLS_FILE_PATH.split('.')[0] + '-' + str(datetime.datetime.today().strftime('%Y-%m-%d').split()[0]) + '.' + XLS_FILE_PATH.split('.')[1])
    workbook.close()

    print(XLS_FILE_PATH + ' has been successfully created')


def upload_xls():
    site_url = config.get('ckan.site_url')
    if os.path.isfile(XLS_FILE_PATH):
        xls_file_url = site_url + '/statistics_xls/' + XLS_FILE_NAME

        pkg = requests.get(site_url + '/api/action/package_show?id=' + XLS_PACKAGE_ID).json()['result']

        if len(pkg['resources']) > 0:
            xls_resource = None
            for resource in pkg['resources']:
                if resource['name'] == XLS_RESOURCE_NAME:
                    xls_resource = resource

            if xls_resource is None:
                create_resource(xls_file_url)
            else:
                update_resource(xls_resource, xls_file_url)
        else:
            create_resource(xls_file_url)
    else:
        print("error: xls-file doesn't exist. Please create the xls file before trying to upload it.")


def update_resource(xls_resource, xls_file_url):
    site_url = config.get('ckan.site_url')
    response = requests.post(site_url + '/api/action/resource_update',
                             json={
                                 'id': xls_resource['id'],
                                 'url': xls_file_url
                             },
                             headers={
                                 'Authorization': API_KEY,
                             })
    if response.status_code == 200:
        print "xls file of resource has been successfully updated!"
    else:
        print "error: statuscode: ", response.status_code, ". Please check if xls resource exists."


def create_resource(xls_file_url):
    site_url = config.get('ckan.site_url')
    response = requests.post(site_url + '/api/action/resource_create',
                             json={
                                 'package_id': XLS_PACKAGE_ID,
                                 'name': XLS_RESOURCE_NAME,
                                 'url': xls_file_url
                             },
                             headers={
                                 'Authorization': API_KEY,
                             })
    if response.status_code == 200:
        print "resource with xls_file has been successfully created!"
    else:
        print "error: statuscode: ", response.status_code, ". Please check if '", XLS_PACKAGE_ID, "' exists."


def change_encoding_xlsx():
    workbook = openpyxl.load_workbook(filename=XLS_FILE_PATH)
    workbook.encoding = 'utf-8'
    workbook.save(XLS_FILE_PATH)
    workbook.close()

    print "changed encoding of xlsx file to utf-8"


def call_all_functions():
    add_latest_to_xls()
    upload_xls()


def delete_xls_file():
    if os.path.isfile(XLS_FILE_PATH):
        os.remove(XLS_FILE_PATH)
