# -*- coding: utf8 -*-

from datetime import datetime as d
import dateutil
import ckan.plugins.toolkit as tk

from ckanext.eodp.model import DatasetsPerCategory, DatasetsPerCatalogue, DatasetsPerCountry, \
    DatasetsPerCountryCatalogue, DatasetsPerCountryCategory, DatasetsAssignedToDataCategoriesPerCountry, \
    DatasetsAssignedToDataCategoriesByCountryCatalogue

from ckan.common import request

from ckanext.eodp.statistics import statistics

import ckan.lib.base as base
import ckan.lib.helpers as h

from ckan.common import _, c
import logging

from multiprocessing import Process

from ckanext.eodp.statistics import *

from collections import Counter

redirect = base.redirect

log = logging.getLogger(__name__)


class StatisticController(tk.BaseController):

    dataset_models = [
        {'title': "Datasets per Category", "model": DatasetsPerCategory},
        {'title': "Datasets per Catalogue", "model": DatasetsPerCatalogue},
        {'title': "Datasets per Country and Catalogue", "model": DatasetsPerCountryCatalogue},
        {'title':  "Datasets per Country", "model": DatasetsPerCountry}
        # {'title': "Datasets per Country and Category", "model": DatasetsPerCountryCategory},
        # {'title': "Datasets assigned to Categories per Country", "model":
        #     DatasetsAssignedToDataCategoriesPerCountry},
        # {'title': "Datasets assigned to Categories by Country and Catalogue", "model":
        #     DatasetsAssignedToDataCategoriesByCountryCatalogue}
    ]

    chart_mapping = {
        1: ['bar', 'multiline'],
        2: ['line', 'multiline'],
        3: ['line', 'multiline'],
        4: ['bar', 'line', 'multiline'],
        5: ['line'],
        6: ['bar', 'line', 'multiline'],
        7: ['line', 'multiline'],
    }


    def allowed_to_update(self):
        if c.userobj:
            return h.check_access('sysadmin') or c.userobj.name in ALLOWED_USERS_FOR_UPDATE
        else:
            return h.check_access('sysadmin')

    def call_model_functions_current(self):
        if 'statistic-models' in request.POST.multi:
            model_index = int(request.POST.multi['statistic-models'])
        else:
            model_index = 1

        dataset_model = self.dataset_models[model_index-1]
        years = self.get_years_for_model(model_index)

        date_begin = years[0]
        date_end = years[len(years)-1]

        timerange = [date_begin, date_end]

        _dataset_model = list(self.dataset_models)
        del _dataset_model[4:]

        return tk.render('statistic/current.html',
                         extra_vars={
                             'years': years,
                             'models': _dataset_model,
                             'model_index': model_index,
                             'date_begin': date_begin,
                             'date_end': date_end,
                             'dataset_model': dataset_model['title'],
                             'data_1': self.get_average_increase(dataset_model['model'], timerange[0], timerange[1]),
                             # 'data_1': self.get_average_increase(dataset_model['model'], "2017-08-02", "2017-08-15"),
                             'data_1_line': self.get_total_increase(dataset_model['model'], timerange[0], timerange[1]),
                             'data_multiple_line': self.get_total_increase_for_labels(dataset_model['model'], timerange[0], timerange[1]),
                             'data_pie': self.return_data_for_year(dataset_model['model'], timerange[1]),
                             'allowed_to_update': self.allowed_to_update(),
                             })

    def call_model_functions_evolution(self):
        if 'model_index' in request.POST.multi:
            model_index = int(request.POST.multi['model_index'])
            dataset_model = self.dataset_models[model_index - 1]

            date_begin = dateutil.parser.parse(request.POST.multi['statistic-date-1'])
            date_end = dateutil.parser.parse(request.POST.multi['statistic-date-2'])

            # if date_begin > date_end:
            #     date_begin = request.POST.multi['statistic-date-2'].split()[0]
            #     date_end = request.POST.multi['statistic-date-1'].split()[0]

            timerange = [date_begin, date_end]

            return tk.render('statistic/evolution.html',
                             extra_vars={
                                 'years': self.get_years_for_dataset_evolution()['years'],
                                 'models': self.dataset_models,
                                 'model_index': model_index,
                                 'date_begin': date_begin,
                                 'date_end': date_end,
                                 'dataset_model': dataset_model['title'],
                                 'data_1': self.get_average_increase(dataset_model['model'], timerange[0],
                                                                     timerange[1]),
                                 'data_1_line': self.get_total_increase(dataset_model['model'], timerange[0],
                                                                        timerange[1]),
                                 'data_multiple_line': self.get_total_increase_for_labels(dataset_model['model'],
                                                                                          timerange[0], timerange[1]),
                                 'data_pie': self.return_data_for_year(dataset_model['model'], timerange[0]),
                                 'allowed_to_update': self.allowed_to_update(),
                                 'charts': self.chart_mapping[model_index]
                             })

        else:
            result = self.get_years_for_dataset_evolution()
            return tk.render('statistic/evolution.html',
                             extra_vars={
                                 'model_index': result['model_index'],
                                 'models': self.dataset_models,
                                 'years': result['years'],
                                 'date_begin': result['years'][0],
                                 'date_end': result['years'][len(result['years']) - 1],
                                 'allowed_to_update': self.allowed_to_update()
                             }
                             )

    def prepare_statistics_selection(self):
        return tk.render('statistic/evolution.html',
                         extra_vars={
                             'models': self.dataset_models,
                             'allowed_to_update': self.allowed_to_update()
                         }
                         )

    def get_years_for_model(self, model_index):
        dataset_model = self.dataset_models[model_index - 1]
        # test = dataset_model['model'].get_distinct_dates()
        # data_list = dataset_model['model'].get_data()
        #
        # label_count = len([item for item in data_list if item.label == data_list[0].label])
        #
        # datasets = sorted(data_list, key=lambda key: key.label)
        #
        # years = []
        #
        # for i in range(0, label_count):
        #     years.append(str(datasets[i].date).split()[0])

        return dataset_model['model'].get_distinct_dates()

    def get_years_for_dataset_evolution(self):
        if 'statistic-models' in request.POST.multi:
            model_index = int(request.POST.multi['model_index'])
        else:
            model_index = 1

        years = self.get_years_for_model(model_index)

        return {'model_index': model_index,
                'models': self.dataset_models,
                'years': years,
                'date_begin': years[0],
                'date_end': years[len(years)-1]}

    def return_data_for_year(self, dataset_model, date):
        datasets_from_year = dataset_model.get_by_date(date)

        result_labels = []
        result_data = []

        label_count = len([item for item in datasets_from_year if item.label == datasets_from_year[0].label])
        datasets_from_year = self.remove_labels_with_fewer_count(datasets_from_year, label_count)

        for item in datasets_from_year:
            result_labels.append(_(item.label))

            try:
                result_data.append({'total_count': item.total_count, 'assigned_count': item.assigned_count})
            except:
                result_data.append(item.count)

        return {'labels': result_labels, 'data': result_data, 'date': date.strftime('%d-%m-%Y')}

    def get_datasets_between_years(self, datasets, start_date, end_date):
        result = []
        for k in range(0, len(datasets)):
            if start_date <= datasets[k].date <= end_date:
                result.append(datasets[k])
            else:
                continue
        return result

    def get_average_increase(self, dataset_model, start_date, end_date):
        start_datasets = dataset_model.get_by_date(start_date)

        result_labels = []
        result_data = []

        for start_dataset in start_datasets:
            try:
                end_dataset = dataset_model.get_by_date_and_label(end_date, start_dataset.label)
                result_labels.append(_(start_dataset.label))
                value = round(((float(end_dataset.get_count()) / float(start_dataset.get_count())) - 1) * 100, 2)
                result_data.append(value)
            except:
                pass

        return {'labels': result_labels, 'data': result_data, 'start_date': start_date.strftime('%d-%m-%Y'), 'end_date': end_date.strftime('%d-%m-%Y'),
                'highest_value': max(result_data)}

    def get_total_increase_for_labels(self, dataset_model, start_date, end_date):
        data_list = dataset_model.get_data()

        datasets_between = self.get_datasets_between_years(data_list, start_date, end_date)

        label_count = len([item for item in datasets_between if item.label == datasets_between[0].label])

        datasets_between = self.remove_labels_with_fewer_count(datasets_between, label_count)

        datasets_between_date = sorted(datasets_between, key=lambda key: key.date)
        datasets_between_label = sorted(datasets_between_date, key=lambda key: key.label)

        i = 0
        result_labels = []
        result_data = []

        for l in range(0, label_count):
            result_labels.append(str(datasets_between_label[l].date).split()[0])

        while i < len(datasets_between_label):
            data = []
            label = _(datasets_between_label[i].label)

            total_count_for_date = 0

            for j in range(0, label_count):
                try:
                    total_count_for_date = datasets_between_label[i].assigned_count
                except:
                    total_count_for_date = datasets_between_label[i].count

                data.append(total_count_for_date)
                i += 1

            result_data.append({"label": label, "data": data})

        return {'labels': result_labels, 'data': result_data, 'start_date': start_date.strftime('%d-%m-%Y'), 'end_date': end_date.strftime('%d-%m-%Y')}

    def get_total_increase(self, dataset_model, start_date, end_date):
        data_list = dataset_model.get_data()

        result_labels = []
        result_data = []

        datasets_between = self.get_datasets_between_years(data_list, start_date, end_date)

        label_count = len([item for item in datasets_between if item.label == datasets_between[0].label])

        datasets_between = self.remove_labels_with_fewer_count(datasets_between, label_count)

        date_count = len([item for item in datasets_between if item.date == datasets_between[0].date])

        datasets_between = sorted(datasets_between, key=lambda key: key.date)

        i = 0

        while i < len(datasets_between):
            total_count_for_date = 0
            result_labels.append(str(datasets_between[i].date).split()[0])

            for j in range(0, date_count):
                try:
                    total_count_for_date += datasets_between[i].assigned_count
                except:
                    total_count_for_date += datasets_between[i].count

                i += 1
            result_data.append(total_count_for_date)

        return {'labels': result_labels, 'data': result_data, 'label': _('Total Number of Datasets'),
                'start_date': start_date.strftime('%d-%m-%Y'), 'end_date': end_date.strftime('%d-%m-%Y')}

    def get_latest_entries(self, datasets):
        latest_entries = []

        datasets = sorted(datasets, key=lambda key: key.date, reverse=True)

        current_date = datasets[0].date
        latest_datasets = []
        for k in range(0, len(datasets)):
            if current_date != datasets[k].date:
                break
            else:
                latest_datasets.append(datasets[k])

        label_count = len([item for item in latest_datasets if item.label == latest_datasets[0].label])

        latest_datasets = self.remove_labels_with_fewer_count(latest_datasets, label_count)

        latest_datasets = sorted(latest_datasets, key=lambda key: key.label)

        for l in range(0, len(latest_datasets)):
            try:
                latest_entries.append(
                    {"date": latest_datasets[l].date, "label": _(latest_datasets[l].label).encode('utf-8'),
                     "total_count": latest_datasets[l].total_count,
                     "assigned_count": latest_datasets[l].assigned_count})
            except:
                latest_entries.append({"date": latest_datasets[l].date, "label": _(latest_datasets[l].label).encode('utf-8'),
                                       "count": latest_datasets[l].count})

        return latest_entries

    def call_all_functions_with_xls(self):
        if self.allowed_to_update():
            statistics.start_call_all_functions_process()
            redirect(h.url_for(controller="ckanext.eodp.statistics.statistic_controller:StatisticController",
                               action="call_model_functions_current", updating=True))
        else:
            log.error("Logged in user is not allowed to update the statistics")

    def update_statistics(self):
        if self.allowed_to_update():
            Process(target=call_all_functions_with_xls, args=()).start()
            redirect(h.url_for(controller="ckanext.eodp.statistics.statistic_controller:StatisticController",
                               action="call_model_functions_current", updating=True))
        else:
            log.error("Logged in user is not allowed to update the statistics")

    def remove_labels_with_fewer_count(self, datasets_list, label_count):
        counter = Counter()
        for dataset in datasets_list:
            counter[dataset.label] += 1

        remove_ids = []

        for elem in list(counter):
            if counter[elem] < label_count:
                for i in range(0, len(datasets_list)):
                    if datasets_list[i].label == elem:
                        remove_ids.append(datasets_list[i].id)

        for i in range(0, len(remove_ids)):
            for j in range(0, len(datasets_list)):
                if datasets_list[j].id == remove_ids[i]:
                    datasets_list.pop(j)
                    break

        return datasets_list


def call_all_functions_with_xls():
    statistics.call_all_functions_with_xls()
