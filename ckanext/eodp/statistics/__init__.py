from pylons import config
from ckanext.eodp.config import CONFIG

API_KEY = CONFIG.get('statistics', 'api_key')
XLS_PACKAGE_ID = CONFIG.get('statistics', 'package_id')
SITE_URL = config.get('ckan.site_url')
BASE_URL = CONFIG.get('translation', 'callback_base')
XLS_FILE_NAME = CONFIG.get('statistics', 'xls_file_name')
XLS_FILE_PATH = CONFIG.get('statistics', 'xls_file_path')
XLS_RESOURCE_NAME = CONFIG.get('statistics', 'resource_name')
ALLOWED_USERS_FOR_UPDATE = CONFIG.get('statistics', 'allowed_users_for_update').split(" ")
