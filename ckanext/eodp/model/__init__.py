import logging
import json

from sqlalchemy import Table
from sqlalchemy import Column
from sqlalchemy import UniqueConstraint
from sqlalchemy import types
from sqlalchemy.exc import IntegrityError
from sqlalchemy import or_
from ckan import model
from ckan.plugins import toolkit as tk
from ckan.logic.action.get import organization_list

from ckan.model.meta import metadata, Session, mapper
from ckan.model.types import make_uuid
from ckan.model.domain_object import DomainObject

import sqlalchemy.orm as orm
from ckan.model.meta import CkanCacheExtension, CkanSessionExtension
from ckan.model import extension
import ckan.lib.activity_streams_session_extension as activity

import datetime

log = logging.getLogger(__name__)

__all__ = [
    'CountryOrganization', 'country_organization_table',
    'UserQuery', 'user_query_table',
    'DatasetsPerCategory', 'datasets_per_category_table',
    'DatasetsPerCatalogue', 'datasets_per_catalogue_table',
    'DatasetsPerCountryCatalogue', 'datasets_per_country_catalogue_table',
    'DatasetsPerCountry', 'datasets_per_country_table',
    'DatasetsPerCountryCategory', 'datasets_per_country_category_table',
    'DatasetsAssignedToDataCategoriesPerCountry', 'datasets_assigned_to_data_categories_per_country_table',
    'DatasetsAssignedToDataCategoriesByCountryCatalogue', 'datasets_assigned_to_data_categories_by_country_catalogue_table'
]

country_organization_table = None
user_query_table = None
datasets_per_category_table = None
datasets_per_catalogue_table = None
datasets_per_country_catalogue_table = None
datasets_per_country_table = None
datasets_per_country_category_table = None
datasets_assigned_to_data_categories_per_country_table = None
datasets_assigned_to_data_categories_by_country_catalogue_table = None

session = orm.scoped_session(orm.sessionmaker(
                autoflush=False,
                autocommit=False,
                expire_on_commit=False,
                extension=[CkanCacheExtension(),
                           CkanSessionExtension(),
                           extension.PluginSessionExtension(),
                           activity.DatasetActivitySessionExtension()],
            ))


def setup():
    if country_organization_table is None or user_query_table is None or datasets_per_category_table is None\
            or datasets_per_catalogue_table is None or datasets_per_country_catalogue_table is None\
            or datasets_per_country_table is None or datasets_per_country_category_table is None\
            or datasets_assigned_to_data_categories_per_country_table is None or \
            datasets_assigned_to_data_categories_by_country_catalogue_table is None:
        define_edp_tables()
        log.debug('EDP tables defined in memory')

    if not model.package_table.exists():
        log.debug('EDP table creation deferred')
        return

    if not country_organization_table.exists():
        country_organization_table.create()
        log.debug('EDP tables created')

    if not user_query_table.exists():
        user_query_table.create()
        log.debug('EDP query table created')

    if not datasets_per_category_table.exists():
        datasets_per_category_table.create()
        log.debug('EDP datasets_per_category table created')

    if not datasets_per_catalogue_table.exists():
        datasets_per_catalogue_table.create()
        log.debug('EDP datasets_per_catalogue table created')

    if not datasets_per_country_catalogue_table.exists():
        datasets_per_country_catalogue_table.create()
        log.debug('EDP datasets_per_country_catalogue table created')

    if not datasets_per_country_table.exists():
        datasets_per_country_table.create()
        log.debug('EDP datasets_per_country table created')

    if not datasets_per_country_category_table.exists():
        datasets_per_country_category_table.create()
        log.debug('EDP datasets_per_country_category table created')

    if not datasets_assigned_to_data_categories_per_country_table.exists():
        datasets_assigned_to_data_categories_per_country_table.create()
        log.debug('EDP datasets_assigned_to_data_categories_per_country table created')

    if not datasets_assigned_to_data_categories_by_country_catalogue_table.exists():
        datasets_assigned_to_data_categories_by_country_catalogue_table.create()
        log.debug('EDP datasets_assigned_to_data_categories_by_country_catalogue table created')


class EdpDomainObject(DomainObject):
    key_attr = 'id'

    @classmethod
    def get(cls, key, default=None, attr=None):
        if attr == None:
            attr = cls.key_attr
        kwds = {attr: key}
        o = cls.filter(**kwds).first()
        if o:
            return o
        else:
            return default

    @classmethod
    def filter(cls, **kwds):
        query = Session.query(cls).autoflush(False)
        return query.filter_by(**kwds)

    @classmethod
    def get_org_names_for_country(cls, country):
        return Session.query(cls).filter(cls.country == country).all()


class CountryOrganization(EdpDomainObject):
    @classmethod
    def delete_table(cls):
        Session.query(cls).delete()
        Session.commit()


class UserQuery(EdpDomainObject):
    @classmethod
    def get_queries_from_user(cls, user_id, search_query, limit, offset, order, order_by):
        session_query = Session.query(cls).filter(cls.user_id == user_id,
                                                  (cls.query_name.contains(search_query) |
                                                   cls.query_description.contains(search_query)))
        if order_by == 'asc':
            return session_query.order_by(order).offset(offset).limit(limit).all()
        elif order_by == 'desc':
            return session_query.order_by(order.desc()).offset(offset).limit(limit).all()
        else:
            return session_query.order_by(order.desc()).offset(offset).limit(limit).all()

    @classmethod
    def get_queries_count(cls, user_id):
        return Session.query(cls).filter(cls.user_id == user_id).count()

    @classmethod
    def create(cls, query_dict):
        new_query = UserQuery(user_id=query_dict['user'], query_name=query_dict['title'], query_text=query_dict['text'],
                              url_params=query_dict['url_params'])
        Session.add(new_query)


class StatisticsObject(EdpDomainObject):
    @classmethod
    def get_data(cls):
        # return Session.query(cls).all()
        Session = session
        return Session.query(cls).order_by(cls.date).all()

    @classmethod
    def get_distinct_dates(cls):
        dates = Session.query(cls.date).order_by(cls.date).distinct().all()
        result = []
        for i in dates:
            result.append(i.date)
        return result

    @classmethod
    def get_by_date(cls, date):
        return Session.query(cls).order_by(cls.label).filter(cls.date == date).all()


    @classmethod
    def get_by_date_and_label(cls, date, label):
        return Session.query(cls).order_by(cls.label).filter(cls.date == date).filter(cls.label == label).one()

    @classmethod
    def delete_table(cls):
        Session.query(cls).delete()
        Session.commit()


class DatasetsPerCategory(StatisticsObject):
    @classmethod
    def create_datasets_per_category(cls, query_dict):
        new_entry = DatasetsPerCategory(date=query_dict['date'], category=query_dict['title'],
                                        count=query_dict['count'])
        print "new_entry ", new_entry
        # Session.add(new_entry)

    def get_count(self):
        return self.count

class DatasetsPerCatalogue(StatisticsObject):
    @classmethod
    def create_datasets_per_catalogue(cls, query_dict):
        new_entry = DatasetsPerCatalogue(date=query_dict['date'], catalogue=query_dict['title'],
                                         count=query_dict['count'])
        print "new_entry ", new_entry
        # Session.add(new_entry)

    def get_count(self):
        return self.count

class DatasetsPerCountryCatalogue(StatisticsObject):
    @classmethod
    def create_datasets_per_country_catalogue(cls, query_dict):
        new_entry = DatasetsPerCountryCatalogue(date=query_dict['date'], title=query_dict['title'],
                                                count=query_dict['count'])
        print "new_entry ", new_entry
        # Session.add(new_entry)

    def get_count(self):
        return self.count


class DatasetsPerCountry(StatisticsObject):
    @classmethod
    def create_datasets_per_country(cls, query_dict):
        new_entry = DatasetsPerCountry(date=query_dict['date'], country=query_dict['title'],
                                       count=query_dict['count'])
        print "new_entry ", new_entry
        # Session.add(new_entry)

    def get_count(self):
        return self.count


class DatasetsPerCountryCategory(StatisticsObject):
    @classmethod
    def create_datasets_per_country_category(cls, query_dict):
        new_entry = DatasetsPerCountryCategory(date=query_dict['date'], title=query_dict['title'],
                                               count=query_dict['count'])
        print "new_entry ", new_entry
        # Session.add(new_entry)

    def get_count(self):
        return self.count


class DatasetsAssignedToDataCategoriesPerCountry(StatisticsObject):
    @classmethod
    def create_datasets_assigned_to_data_categories_per_country(cls, query_dict):
        new_entry = DatasetsAssignedToDataCategoriesPerCountry(date=query_dict['date'], title=query_dict['title'],
                                                               total_count=query_dict['total_count'],
                                                               assigned_count=query_dict['assigned_count'])
        print "new_entry ", new_entry
        # Session.add(new_entry)

    def get_count(self):
        return self.assigned_count

class DatasetsAssignedToDataCategoriesByCountryCatalogue(StatisticsObject):
    @classmethod
    def create_datasets_assigned_to_data_categories_by_country_catalogue(cls, query_dict):
        new_entry = DatasetsAssignedToDataCategoriesByCountryCatalogue(date=query_dict['date'], title=query_dict['title'],
                                                                       total_count=query_dict['total_count'],
                                                                       assigned_count=query_dict['assigned_count'])
        print "new_entry ", new_entry
        # Session.add(new_entry)

    def get_count(self):
        return self.assigned_count


def define_edp_tables():

    global country_organization_table, user_query_table, datasets_per_category_table, datasets_per_catalogue_table,\
        datasets_per_country_catalogue_table, datasets_per_country_table, datasets_per_country_category_table, \
        datasets_assigned_to_data_categories_per_country_table, \
        datasets_assigned_to_data_categories_by_country_catalogue_table

    country_organization_table = Table('edp_country_organization', metadata,
                                       Column('id', types.UnicodeText, primary_key=True, default=make_uuid),
                                       Column('country', types.UnicodeText, nullable=False),
                                       Column('org_name', types.UnicodeText, nullable=False),
                                       Column('org_id', types.UnicodeText, nullable=False, unique=True),
                                       )

    user_query_table = Table('edp_user_query', metadata,
                             Column('id', types.UnicodeText, primary_key=True, default=make_uuid),
                             Column('user_id', types.UnicodeText, nullable=False),
                             Column('query_name', types.UnicodeText, nullable=False),
                             Column('query_description', types.UnicodeText, nullable=False),
                             Column('url_params', types.UnicodeText, nullable=False),
                             Column('date_created', types.DateTime, default=datetime.datetime.now)
                             )

    datasets_per_category_table = Table('edp_datasets_per_category', metadata,
                                        Column('id', types.UnicodeText, primary_key=True, default=make_uuid),
                                        Column('date', types.DateTime, default=datetime.datetime.today().strftime('%Y-%m-%d').split()[0]),
                                        # Column('date', types.DateTime, nullable=False),
                                        Column('label', types.UnicodeText, nullable=False),
                                        Column('count', types.Integer, nullable=False),
                                        )

    datasets_per_catalogue_table = Table('edp_datasets_per_catalogue', metadata,
                                        Column('id', types.UnicodeText, primary_key=True, default=make_uuid),
                                        Column('date', types.DateTime,
                                               default=datetime.datetime.today().strftime('%Y-%m-%d').split()[0]),
                                        # Column('date', types.DateTime, nullable=False),
                                        Column('label', types.UnicodeText, nullable=False),
                                        Column('count', types.Integer, nullable=False),
                                        )

    datasets_per_country_catalogue_table = Table('edp_datasets_per_country_catalogue', metadata,
                                         Column('id', types.UnicodeText, primary_key=True, default=make_uuid),
                                         Column('date', types.DateTime,
                                                default=datetime.datetime.today().strftime('%Y-%m-%d').split()[0]),
                                         # Column('date', types.DateTime, nullable=False),
                                         Column('label', types.UnicodeText, nullable=False),
                                         Column('count', types.Integer, nullable=False),
                                         )

    datasets_per_country_table = Table('edp_datasets_per_country', metadata,
                                         Column('id', types.UnicodeText, primary_key=True, default=make_uuid),
                                         Column('date', types.DateTime,
                                                default=datetime.datetime.today().strftime('%Y-%m-%d').split()[0]),
                                         # Column('date', types.DateTime, nullable=False),
                                         Column('label', types.UnicodeText, nullable=False),
                                         Column('count', types.Integer, nullable=False),
                                         )

    datasets_per_country_category_table = Table('edp_datasets_per_country_category', metadata,
                                       Column('id', types.UnicodeText, primary_key=True, default=make_uuid),
                                       Column('date', types.DateTime,
                                              default=datetime.datetime.today().strftime('%Y-%m-%d').split()[0]),
                                       # Column('date', types.DateTime, nullable=False),
                                       Column('label', types.UnicodeText, nullable=False),
                                       Column('count', types.Integer, nullable=False),
                                       )

    datasets_assigned_to_data_categories_per_country_table = \
        Table('edp_datasets_assigned_to_data_categories_per_country', metadata,
              Column('id', types.UnicodeText, primary_key=True, default=make_uuid),
              Column('date', types.DateTime,
                     default=datetime.datetime.today().strftime('%Y-%m-%d').split()[0]),
              # Column('date', types.DateTime, nullable=False),
              Column('label', types.UnicodeText, nullable=False),
              Column('total_count', types.Integer, nullable=False),
              Column('assigned_count', types.Integer, nullable=False),
              )

    datasets_assigned_to_data_categories_by_country_catalogue_table = \
        Table('edp_datasets_assigned_to_data_categories_by_country_catalogue', metadata,
              Column('id', types.UnicodeText, primary_key=True, default=make_uuid),
              Column('date', types.DateTime,
                     default=datetime.datetime.today().strftime('%Y-%m-%d').split()[0]),
              # Column('date', types.DateTime, nullable=False),
              Column('label', types.UnicodeText, nullable=False),
              Column('total_count', types.Integer, nullable=False),
              Column('assigned_count', types.Integer, nullable=False),
              )

    mapper(
        CountryOrganization,
        country_organization_table
    )

    mapper(
        UserQuery,
        user_query_table
    )

    mapper(
        DatasetsPerCategory,
        datasets_per_category_table
    )

    mapper(
        DatasetsPerCatalogue,
        datasets_per_catalogue_table
    )

    mapper(
        DatasetsPerCountryCatalogue,
        datasets_per_country_catalogue_table
    )

    mapper(
        DatasetsPerCountry,
        datasets_per_country_table
    )

    mapper(
        DatasetsPerCountryCategory,
        datasets_per_country_category_table
    )

    mapper(
        DatasetsAssignedToDataCategoriesPerCountry,
        datasets_assigned_to_data_categories_per_country_table
    )

    mapper(
        DatasetsAssignedToDataCategoriesByCountryCatalogue,
        datasets_assigned_to_data_categories_by_country_catalogue_table
    )


def init_country_data():
    orgs = organization_list({}, {
        'all_fields': True,
        'include_extras': True
    })
    for o in orgs:
        if 'spatial' in o:
            try:
                country_organization = CountryOrganization()
                country_organization.country = o['spatial'][0]
                country_organization.org_name = o['name']
                country_organization.org_id = o['id']
                country_organization.save()
            except IntegrityError as e:
                log.error(e.message)
                model.Session.rollback()
        else:
            try:
                country_organization = CountryOrganization()
                country_organization.country = 'EU'
                country_organization.org_name = o['name']
                country_organization.org_id = o['id']
                country_organization.save()
            except IntegrityError as e:
                log.error(e.message)
                model.Session.rollback()


def drop_country_data():
    CountryOrganization.delete_table()