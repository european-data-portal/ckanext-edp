__author__ = 'fki'

from ckanext.eodp.config import CONFIG
from ckan.lib import helpers
import urllib
import ckan.plugins.toolkit as tk

import logging
log = logging.getLogger(__name__)


geo_formats = CONFIG.getlist('maps', 'geo_formats')
maps_base_url = CONFIG.get('maps', 'map_apps_base_url')

FORMATS = {}
for geo_format in geo_formats:
    f = geo_format.split('=')
    FORMATS[f[0].lower()] = f[0]
    for item in f[1:]:
        FORMATS[item.lower()] = f[0]

def get_res_map_data(resource, pkg_dict):
    """
    :type resource: dict
    :type pkg_dict: dict
    :rtype: dict
    :param res:
    :return:
    """
    format_name = resource['format'].lower()
    if format_name in FORMATS.keys():
        return {
            'link': _build_map_link(resource['id'], FORMATS[format_name]),
            'name': tk._('Open Geo-Visualization')
        }
    return None


def get_pkg_spatial_data(pkg_dict):
    """
    :type pkg_dict. dict
    :param pkg_dict:
    :return:
    """
    spatial = helpers.get_pkg_dict_extra(pkg_dict, 'spatial')
    if spatial:
        return {
            'link': _build_map_link(pkg_dict['id'], 'GeoJSON'),
            'name': tk._('Open Geo-Visualization')
        }
    else:
        return None


def _build_map_link(id, format_name):
    """
    :type pkg_dict: dict
    :type format_name: str
    :rtype: str
    :param pkg_dict:
    :param format:
    :return:
    """
    parameters = {
        'lang': helpers.lang(),
        'type': format_name,
        'dataset': id
    }
    return maps_base_url + '?' + urllib.urlencode(parameters)


def pkg_is_geo_enabled(data_dict):
    """
    :type data_dict: dict
    :rtype: bool
    :param data_dict:
    :return:
    """
    resources = data_dict['resources']
    for r in resources:
        if r['format'] in geo_formats:
            return True
    spatial = helpers.get_pkg_dict_extra(data_dict, 'spatial')
    if spatial:
        return True
    return False
