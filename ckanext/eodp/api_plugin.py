import ckan.plugins as plugins
import ckan.plugins.toolkit as tk
import ckan.logic.action.create as create
import ckan.logic.action.update as update
import ckan.logic.action.delete as delete
import ckan.logic.action.get as get
from schema import helper as schema_helper
import ckanext.eodp.util as u
import ckanext.eodp.sparql_store.crud as sparql_store
from ckanext.eodp import translation
from ckanext.eodp.trans import actions as trans_actions
from ckanext.eodp.trans import auto_translation
import ckan.logic as logic
import ckan.lib.navl.dictization_functions
from paste.deploy.converters import asbool
from ckan.logic.action.get import _unpick_search
import sqlalchemy
_or_ = sqlalchemy.or_
_and_ = sqlalchemy.and_
_validate = ckan.lib.navl.dictization_functions.validate

import logging
log = logging.getLogger(__name__)


class APIPlugin(plugins.SingletonPlugin):

    plugins.implements(plugins.IActions)

    def get_actions(self):
        return {
            'organization_create': organization_create,
            'organization_update': organization_update,
            'organization_delete': organization_delete,
            'organization_list': organization_list,
            'package_create': package_create,
            'package_update': package_update,
            'package_delete': package_delete,
            'package_show': package_show,
            'package_patch': package_patch,
            'dataset_purge': dataset_purge,
            'resource_create': resource_create,
            'resource_update': resource_update,
            'resource_delete': resource_delete,
            'package_translate': trans_actions.package_translate
        }


@tk.side_effect_free
def organization_create(context, data_dict):
    if not u.is_api_call(context):
        schema_helper.prepare_org_schema_by_view(data_dict)

    schema_helper.preprocess_organization_dict(data_dict)
    org = create.organization_create(context, data_dict)
    log.info("Called custom organization_create: " + org['title'])

    if sparql_store.connected():
        try:
            sparql_store.create_organization(org)
        except Exception as e:
            log.warning("sparql sync (" + org['title'] + ") " + e.message.decode('utf-8'))

    return org


@tk.side_effect_free
def organization_update(context, data_dict):
    if not u.is_api_call(context):
        schema_helper.prepare_org_schema_by_view(data_dict)

    schema_helper.preprocess_organization_dict(data_dict)
    org = update.organization_update(context, data_dict)
    log.info("Called custom organization_update: " + org['title'])

    if sparql_store.connected():
        try:
            sparql_store.update_organization(org)
        except Exception as e:
            log.warning("sparql sync (" + org['title'] + ") " + e.message.decode('utf-8'))

    return org


@tk.side_effect_free
def organization_delete(context, data_dict):
    model = context['model']
    organization = model.Group.get(data_dict['id'])

    delete.organization_delete(context, data_dict)
    log.info("Called custom organization_delete: " + organization.name)

    if sparql_store.connected():
        try:
            sparql_store.delete_organization(organization.name)
        except Exception as e:
            log.warning("sparql sync (" + data_dict['id'] + ") " + e.message.decode('utf-8'))


@tk.side_effect_free
def organization_list(context, data_dict):

    is_org = True
    tk.check_access('organization_list', context, data_dict)
    data_dict['groups'] = data_dict.pop('organizations', [])
    data_dict['type'] = 'organization'

    model = context['model']
    api = context.get('api_version')
    groups = data_dict.get('groups')
    group_type = data_dict.get('type', 'group')
    ref_group_by = 'id' if api == 2 else 'name'
    pagination_dict = {}
    limit = data_dict.get('limit')
    if limit:
        pagination_dict['limit'] = data_dict['limit']
    offset = data_dict.get('offset')
    if offset:
        pagination_dict['offset'] = data_dict['offset']
    if pagination_dict:
        pagination_dict, errors = _validate(
            data_dict, logic.schema.default_pagination_schema(), context)
        if errors:
            raise tk.ValidationError(errors)
    sort = data_dict.get('sort') or 'name'
    q = data_dict.get('q')
    spatial = [p[1] for p in tk.request.params.items() if p[0] == 'spatial']

    all_fields = asbool(data_dict.get('all_fields', None))

    # order_by deprecated in ckan 1.8
    # if it is supplied and sort isn't use order_by and raise a warning
    order_by = data_dict.get('order_by', '')
    if order_by:
        log.warn('`order_by` deprecated please use `sort`')
        if not data_dict.get('sort'):
            sort = order_by

    # if the sort is packages and no sort direction is supplied we want to do a
    # reverse sort to maintain compatibility.
    if sort.strip() in ('packages', 'package_count'):
        sort = 'package_count desc'

    sort_info = _unpick_search(sort,
                               allowed_fields=['name', 'packages',
                                               'package_count', 'title'],
                               total=1)

    if sort_info and sort_info[0][0] == 'package_count':
        query = model.Session.query(model.Group.id,
                                    model.Group.name,
                                    sqlalchemy.func.count(model.Group.id))

        query = query.filter(model.Member.group_id == model.Group.id) \
            .filter(model.Member.table_id == model.Package.id) \
            .filter(model.Member.table_name == 'package') \
            .filter(model.Package.state == 'active')
    else:
        query = model.Session.query(model.Group.id,
                                    model.Group.name)

    query = query.filter(model.Group.state == 'active')

    if groups:
        query = query.filter(model.Group.name.in_(groups))
    if q:
        q = u'%{0}%'.format(q)
        query = query.filter(_or_(
            model.Group.name.ilike(q),
            model.Group.title.ilike(q),
            model.Group.description.ilike(q)
        ))

    if spatial:
        if isinstance(spatial, unicode):
            spatial = u'%{0}%'.format(spatial)
            query = query.join(model.GroupExtra)
            query = query.filter(_and_(
                model.GroupExtra.key == 'spatial',
                model.GroupExtra.value.contains(spatial),
            ))

        if isinstance(spatial, list):
            query = query.outerjoin(model.GroupExtra, _and_(model.Group.id == model.GroupExtra.group_id,
                                                            model.GroupExtra.key == 'spatial'))
            clauses = []
            for s in spatial:
                if s == u'EU':
                    _query = query.filter(
                        model.GroupExtra.key == None,
                    )
                    query_all = _query.all()

                    for entry in query_all:
                        clauses.append(model.Group.id.contains(entry.id))
                else:
                    spa = u'%{0}%'.format(s)
                    clauses.append(model.GroupExtra.value.contains(spa))
            query = query.filter(_or_(*clauses))

    query = query.filter(model.Group.is_organization == is_org)
    if not is_org:
        query = query.filter(model.Group.type == group_type)
    if sort_info:
        sort_field = sort_info[0][0]
        sort_direction = sort_info[0][1]
        if sort_field == 'package_count':
            query = query.group_by(model.Group.id, model.Group.name)
            sort_model_field = sqlalchemy.func.count(model.Group.id)
        elif sort_field == 'name':
            sort_model_field = model.Group.name
        elif sort_field == 'title':
            sort_model_field = model.Group.title

        if sort_direction == 'asc':
            query = query.order_by(sqlalchemy.asc(sort_model_field))
        else:
            query = query.order_by(sqlalchemy.desc(sort_model_field))

    if limit:
        query = query.limit(limit)
    if offset:
        query = query.offset(offset)

    groups = query.all()

    if all_fields:
        action = 'organization_show' if is_org else 'group_show'
        group_list = []
        for group in groups:
            data_dict['id'] = group.id
            for key in ('include_extras', 'include_tags', 'include_users',
                        'include_groups', 'include_followers'):
                if key not in data_dict:
                    data_dict[key] = False

            group_list.append(logic.get_action(action)(context, data_dict))
    else:
        group_list = [getattr(group, ref_group_by) for group in groups]

    return group_list


@tk.side_effect_free
def package_create(context, data_dict):
    if not u.is_api_call(context):
        schema_helper.prepare_schema_by_view(data_dict)

    schema_helper.preprocess_data_dict(data_dict)
    auto_translate = auto_translation.before_package_create(data_dict)
    pkg = create.package_create(context, data_dict)
    # log.info("Called custom package_create: " + pkg['title'])

    if auto_translate:
        auto_translation.after_package_create(context, pkg)

    if pkg['state'] == 'active' and sparql_store.connected():
        try:
            sparql_store.create_dataset(pkg)
        except Exception as e:
            log.warning("sparql sync (" + pkg['title'] + ") " + e.message.decode('utf-8'))

    return pkg


@tk.side_effect_free
def package_update(context, data_dict):

    if not u.is_api_call(context):
        translation.update(context, data_dict)
        schema_helper.prepare_schema_by_view(data_dict)

    schema_helper.preprocess_data_dict(data_dict)
    auto_translate = auto_translation.before_package_update(data_dict, context)
    pkg = update.package_update(context, data_dict)
    # log.info("Called custom package_update: " + pkg['title'])

    if auto_translate:
        auto_translation.after_package_create(context, pkg)

    if pkg['state'] == 'active' and sparql_store.connected():
        try:
            sparql_store.update_dataset(pkg)
        except Exception as e:
            log.warning("sparql sync (" + pkg['title'] + ") " + e.message.decode('utf-8'))

    return pkg


@tk.side_effect_free
def package_patch(context, data_dict):
    '''Patch a dataset (package).

    :param id: the id or name of the dataset
    :type id: string

    The difference between the update and patch methods is that the patch will
    perform an update of the provided parameters, while leaving all other
    parameters unchanged, whereas the update methods deletes all parameters
    not explicitly provided in the data_dict

    You must be authorized to edit the dataset and the groups that it belongs
    to.
    '''

    tk.check_access('package_patch', context, data_dict)

    show_context = {
        'model': context['model'],
        'session': context['session'],
        'user': context['user'],
        'auth_user_obj': context['auth_user_obj'],
    }

    package_dict = tk.get_action('package_show')(
        show_context,
        {'id': tk.get_or_bust(data_dict, 'id')})

    patched = dict(package_dict)
    patched.update(data_dict)
    patched['id'] = package_dict['id']
    schema_helper.preprocess_data_dict(patched)

    pkg = update.package_update(context, patched)

    if pkg['state'] == 'active' and sparql_store.connected():
        try:
            sparql_store.update_dataset(pkg)
        except Exception as e:
            log.warning("sparql sync (" + pkg['title'] + ") " + e.message.decode('utf-8'))

    return pkg;


@tk.side_effect_free
def package_show(context, data_dict):
    pkg = get.package_show(context, data_dict)

    # if not u.is_api_call(context) and not save:
    #     schema_helper.prepare_schema_for_view(pkg)

    # log.info("Called custom package_show: " + pkg['title'])
    return pkg


@tk.side_effect_free
def package_delete(context, data_dict):
    model = context['model']
    pkg = model.Package.get(data_dict['id'])

    delete.package_delete(context, data_dict)
    log.info("Called custom package_delete: " + data_dict['id'])

    if sparql_store.connected():
        try:
            org = model.Group.get(pkg.owner_org)
            sparql_store.delete_dataset(pkg.name, org.name)
        except Exception as e:
            log.warning("sparql sync (" + pkg.name + ") " + e.message.decode('utf-8'))


@tk.side_effect_free
def dataset_purge(context, data_dict):
    model = context['model']
    pkg = model.Package.get(data_dict['id'])

    delete.dataset_purge(context, data_dict)
    log.info("Called custom dataset_purge: " + data_dict['id'])

    if sparql_store.connected():
        try:
            org = model.Group.get(pkg.owner_org)
            sparql_store.delete_dataset(pkg.name, org.name)
        except Exception as e:
            log.warning("sparql sync (" + pkg.name + ") " + e.message.decode('utf-8'))


@tk.side_effect_free
def resource_create(context, data_dict):
    if not u.is_api_call(context):
        schema_helper.prepare_resource_by_view(data_dict)

    resource = create.resource_create(context, data_dict)
    log.info("Called custom resource_create: " + resource['name'])

    if sparql_store.connected():
        try:
            sparql_store.create_distribution(resource, context['package'])
        except Exception as e:
            log.warning("sparql sync (" + context['package']['title'] + ") " + e.message.decode('utf-8'))

    return resource


@tk.side_effect_free
def resource_update(context, data_dict):

    if not u.is_api_call(context):
        schema_helper.prepare_resource_by_view(data_dict)

    resource = update.resource_update(context, data_dict)
    log.info("Called custom resource_update: " + resource['name'])

    if sparql_store.connected():
        try:
            sparql_store.update_distribution(resource, context['package'])
        except Exception as e:
            log.warning("sparql sync (" + context['package']['title'] + ") " + e.message.decode('utf-8'))

    return resource


@tk.side_effect_free
def resource_delete(context, data_dict):
    model = context['model']
    resource = model.Resource.get(data_dict['id'])

    delete.resource_delete(context, data_dict)
    log.info("Called custom resource_delete: " + data_dict['id'])

    if sparql_store.connected():
        try:
            sparql_store.delete_distribution(resource.id)
        except Exception as e:
            log.warning("sparql sync (" + data_dict['id'] + ") " + e.message.decode('utf-8'))
