import ckan.plugins.toolkit as tk

import ckan.lib.base as base
import ckan.model as model
from ckan.common import _, c
import ckan.lib.helpers as h

import ckan.logic as logic
import ckan.authz as authz

from ckan.common import request, g

from ckanext.eodp.model import UserQuery


abort = base.abort

get_action = logic.get_action
NotFound = logic.NotFound
NotAuthorized = logic.NotAuthorized

redirect = base.redirect


class QueryController(tk.BaseController):

    def new(self):
        """
        assigns the correct values to the input fields of the form
        :return: renders the query creation page, or calls the save function
        """
        c.query_title = h.request.params.get('query_title')

        c.query_url = h.request.params.get('query_url')
        try:
            c.query_url = h.request.params.get('query_url').split(" u'")[1]
            c.query_url = c.query_url[:-3]
        except:
            c.query_url = "q=" + c.query_title

        context = {'model': model, 'session': model.Session,
                   'user': c.user or c.author, 'auth_user_obj': c.userobj,
                   'save': 'save' in request.params}

        if context['save']:
            return self._save_new()

        return tk.render('query/new.html')

    def _save_new(self):
        user_query = UserQuery()
        user_query.user_id = c.user
        user_query.query_name = request.params.get('title')
        user_query.query_description = request.params.get('notes')[:300]
        user_query.url_params = request.params.get('url')

        user_query.save()

        redirect(h.url_for(controller="ckanext.eodp.query_controller:QueryController",
                           action="dashboard_queries"))

    def delete(self):
        query_id = h.request.params.get('query_id')

        user_query = UserQuery.get(query_id)
        user_query.delete()
        user_query.commit()

        redirect(h.url_for(controller="ckanext.eodp.query_controller:QueryController",
                           action="dashboard_queries"))

    def _setup_template_variables(self, context, data_dict):
        c.is_sysadmin = authz.is_sysadmin(c.user)
        try:
            user_dict = get_action('user_show')(context, data_dict)
        except NotFound:
            abort(404, _('User not found'))
        except NotAuthorized:
            abort(401, _('Not authorized to see this page'))

        c.user_dict = user_dict
        c.is_myself = user_dict['name'] == c.user
        c.about_formatted = h.render_markdown(user_dict['about'])

    def dashboard_queries(self):
        context = {'for_view': True, 'user': c.user or c.author,
                   'auth_user_obj': c.userobj}
        data_dict = {'user_obj': c.userobj}

        self._setup_template_variables(context, data_dict)

        page = self._get_page_number(request.params)

        limit = 10

        offset = (page-1) * limit

        c.sort_by_selected = request.params.get('sort', "date desc")

        def select_order(query_order):
            if query_order == "name asc":
                return UserQuery.query_name, "asc"
            elif query_order == "name desc":
                return UserQuery.query_name, "desc"
            elif query_order == "date desc":
                return UserQuery.date_created, "desc"
            else:
                return UserQuery.date_created, "desc"

        order, order_by = select_order(c.sort_by_selected)

        c.q = request.params.get('q', u'')

        query_array = UserQuery.get_queries_from_user(c.user, c.q, limit, offset, order, order_by)

        c.user_dict['queries'] = query_array

        def get_queries_count():
            if len(c.q) > 0:
                return len(c.user_dict['queries'])
            else:
                return UserQuery.get_queries_count(c.user)

        queries_count = get_queries_count()

        def pager_url(q=None, page=None):
            return h.url_for(controller="ckanext.eodp.query_controller:QueryController",
                             action="dashboard_queries") + "?page=" + str(page) + "&sort=" + str(c.sort_by_selected)
        c.page = h.Page(
            collection=c.user_dict['queries'],
            page=page,
            url=pager_url,
            item_count=queries_count,
            items_per_page=limit
        )

        return tk.render('user/dashboard_queries.html')
