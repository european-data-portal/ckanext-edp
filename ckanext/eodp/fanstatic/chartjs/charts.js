var background_colors = [
        'rgba(0, 174, 239, 0.6)',
        'rgba(44, 53, 129, 0.6)',
        'rgba(12, 113, 182, 0.6)',
        'rgba(249, 160, 27, 0.6)',
        'rgba(242, 104, 41, 0.6)',
        'rgba(20, 194, 255, 0.6)',
        'rgba(64, 73, 149, 0.6)',
        'rgba(32, 133, 202, 0.6)',
        'rgba(255, 180, 47, 0.6)',
        'rgba(255, 124, 61, 0.6)',

        'rgba(0, 114, 180, 0.6)',
        'rgba(0, 0, 69, 0.6)',
        'rgba(0, 53, 122, 0.6)',
        'rgba(189, 100, 0, 0.6)',
        'rgba(182, 44, 0, 0.6)',
        'rgba(0, 114, 175, 0.6)',
        'rgba(0, 0, 69, 0.6)',
        'rgba(0, 53, 122, 0.6)',
        'rgba(175, 100, 0, 0.6)',
        'rgba(175, 44, 0, 0.6)',

        'rgba(0, 194, 250, 0.6)',
        'rgba(64, 73, 149, 0.6)',
        'rgba(32, 133, 202, 0.6)',
        'rgba(249, 180, 47, 0.6)',
        'rgba(250, 124, 61, 0.6)',
        'rgba(40, 214, 255, 0.6)',
        'rgba(84, 93, 169, 0.6)',
        'rgba(52, 153, 222, 0.6)',
        'rgba(255, 200, 67, 0.6)',
        'rgba(255, 144, 81, 0.6)',

        'rgba(0, 94, 160, 0.6)',
        'rgba(0, 0, 49, 0.6)',
        'rgba(0, 33, 102, 0.6)',
        'rgba(169, 80, 0, 0.6)',
        'rgba(162, 24, 0, 0.6)',
        'rgba(0, 94, 155, 0.6)',
        'rgba(0, 0, 49, 0.6)',
        'rgba(0, 33, 102, 0.6)',
        'rgba(155, 80, 0, 0.6)',
        'rgba(155, 24, 0, 0.6)',

        'rgba(0, 154, 219, 0.6)',
        'rgba(24, 33, 109, 0.6)',
        'rgba(0, 93, 162, 0.6)',
        'rgba(229, 140, 07, 0.6)',
        'rgba(222, 84, 21, 0.6)',
        'rgba(0, 154, 215, 0.6)',
        'rgba(24, 33, 109, 0.6)',
        'rgba(0, 93, 162, 0.6)',
        'rgba(215, 140, 7, 0.6)',
        'rgba(215, 84, 21, 0.6)',

        'rgba(0, 74, 140, 0.6)',
        'rgba(0, 0, 29, 0.6)',
        'rgba(0, 13, 82, 0.6)',
        'rgba(149, 60, 0, 0.6)',
        'rgba(142, 0, 0, 0.6)',
        'rgba(0, 74, 135, 0.6)',
        'rgba(0, 0, 29, 0.6)',
        'rgba(0, 13, 82, 0.6)',
        'rgba(135, 60, 0, 0.6)',
        'rgba(135, 0, 0, 0.6)',

        'rgba(0, 134, 200, 0.6)',
        'rgba(04, 13, 89, 0.6)',
        'rgba(0, 73, 142, 0.6)',
        'rgba(209, 120, 0, 0.6)',
        'rgba(202, 64, 0, 0.6)',
        'rgba(0, 134, 195, 0.6)',
        'rgba(4, 13, 89, 0.6)',
        'rgba(0, 73, 142, 0.6)',
        'rgba(195, 120, 0, 0.6)',
        'rgba(195, 64, 0, 0.6)',

        'rgba(0, 54, 120, 0.6)',
        'rgba(0, 0, 9, 0.6)',
        'rgba(0, 0, 62, 0.6)',
        'rgba(129, 40, 0, 0.6)',
        'rgba(122, 0, 0, 0.6)',
        'rgba(0, 54, 115, 0.6)',
        'rgba(0, 0, 9, 0.6)',
        'rgba(0, 0, 62, 0.6)',
        'rgba(115, 40, 0, 0.6)',
        'rgba(115, 0, 0, 0.6)',
    ];
var border_colors = [
        'rgba(0, 174, 239, 1)',
        'rgba(44, 53, 129, 1)',
        'rgba(12, 113, 182, 1)',
        'rgba(249, 160, 27, 1)',
        'rgba(242, 104, 41, 1)',
        'rgba(20, 194, 255, 1)',
        'rgba(64, 73, 149, 1)',
        'rgba(32, 133, 202, 1)',
        'rgba(255, 180, 47, 1)',
        'rgba(255, 124, 61, 1)',

        'rgba(0, 114, 180, 1)',
        'rgba(0, 0, 69, 1)',
        'rgba(0, 53, 122, 1)',
        'rgba(189, 100, 0, 1)',
        'rgba(182, 44, 0, 1)',
        'rgba(0, 114, 175, 1)',
        'rgba(0, 0, 69, 1)',
        'rgba(0, 53, 122, 1)',
        'rgba(175, 100, 0, 1)',
        'rgba(175, 44, 0, 1)',

        'rgba(0, 194, 250, 1)',
        'rgba(64, 73, 149, 1)',
        'rgba(32, 133, 202, 1)',
        'rgba(249, 180, 47, 1)',
        'rgba(250, 124, 61, 1)',
        'rgba(40, 214, 255, 1)',
        'rgba(84, 93, 169, 1)',
        'rgba(52, 153, 222, 1)',
        'rgba(255, 200, 67, 1)',
        'rgba(255, 144, 81, 1)',

        'rgba(0, 94, 160, 1)',
        'rgba(0, 0, 49, 1)',
        'rgba(0, 33, 102, 1)',
        'rgba(169, 80, 0, 1)',
        'rgba(162, 24, 0, 1)',
        'rgba(0, 94, 155, 1)',
        'rgba(0, 0, 49, 1)',
        'rgba(0, 33, 102, 1)',
        'rgba(155, 80, 0, 1)',
        'rgba(155, 24, 0, 1)',

        'rgba(0, 154, 219, 1)',
        'rgba(24, 33, 109, 1)',
        'rgba(0, 93, 162, 1)',
        'rgba(229, 140, 07, 1)',
        'rgba(222, 84, 21, 1)',
        'rgba(0, 154, 215, 1)',
        'rgba(24, 33, 109, 1)',
        'rgba(0, 93, 162, 1)',
        'rgba(215, 140, 7, 1)',
        'rgba(215, 84, 21, 1)',

        'rgba(0, 74, 140, 1)',
        'rgba(0, 0, 29, 1)',
        'rgba(0, 13, 82, 1)',
        'rgba(149, 60, 0, 1)',
        'rgba(142, 0, 0, 1)',
        'rgba(0, 74, 135, 1)',
        'rgba(0, 0, 29, 1)',
        'rgba(0, 13, 82, 1)',
        'rgba(135, 60, 0, 1)',
        'rgba(135, 0, 0, 1)',

        'rgba(0, 134, 200, 1)',
        'rgba(04, 13, 89, 1)',
        'rgba(0, 73, 142, 1)',
        'rgba(209, 120, 0, 1)',
        'rgba(202, 64, 0, 1)',
        'rgba(0, 134, 195, 1)',
        'rgba(4, 13, 89, 1)',
        'rgba(0, 73, 142, 1)',
        'rgba(195, 120, 0, 1)',
        'rgba(195, 64, 0, 1)',

        'rgba(0, 54, 120, 1)',
        'rgba(0, 0, 9, 1)',
        'rgba(0, 0, 62, 1)',
        'rgba(129, 40, 0, 1)',
        'rgba(122, 0, 0, 1)',
        'rgba(0, 54, 115, 1)',
        'rgba(0, 0, 9, 1)',
        'rgba(0, 0, 62, 1)',
        'rgba(115, 40, 0, 1)',
        'rgba(115, 0, 0, 1)',
    ];

var bar_chart = function(data, element_id, show){
    if(show == 1){
        var ctx = document.getElementById(element_id);
        var bar_chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: data.labels,
                datasets: [{
                    label: 'Increase in %',
                    data: data.data,
                    backgroundColor: background_colors,
                    borderColor: border_colors,
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    labels: {
                        backgroundColor: "white",
                        borderColor: "black"
                    },
                    fullWidth: true
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            autoSkip: false
                        }
                    }]
                },

                responsive: true,
                maintainAspectRatio: true

            }
        });
    }
};


var pie_chart = function(data, element_id, show){
    if(show == 1) {
        var ctx = document.getElementById(element_id);
        var pie_chart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: data.labels,
                datasets: [{
                    label: '# of Votes',
                    data: data.data,
                    backgroundColor: background_colors,
                    borderColor: border_colors,
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    fullWidth: true
                }
            },
            responsive: true,
            maintainAspectRatio: true
        });
    }
};


var line_chart = function(data, element_id, show){
    if(show == 1) {
        var ctx = document.getElementById(element_id);
        var line_chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: data.labels,
                datasets: [{
                    label: data.label,
                    data: data.data,
                    fill: false,
                    backgroundColor: background_colors,
                    borderColor: border_colors,
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                        ticks: {
                            autoSkip: false
                        }
                    }]
                }
            },
            responsive: true,
            maintainAspectRatio: true
        });
    }
};

var multiple_line_chart = function(data, element_id, show){
    if(show == 1) {
        var ctx = document.getElementById(element_id);
        var line_chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: data.labels,
                datasets: prepare_data_for_multiple_lines(data.data)
            },
            options: {
                scales: {
                    xAxes: [{
                        ticks: {
                            autoSkip: false
                        }
                    }]
                }
            },
            responsive: true,
            maintainAspectRatio: true
        });
    }
};

var prepare_data_for_multiple_lines = function(data){
    var result = [];

    for(var i=0; i < data.length; i++){
        result.push({label: data[i].label, data: data[i].data, fill: false, backgroundColor: background_colors[i],
        borderColor: border_colors[i], borderWidth: 1})
    }

    return result;
};