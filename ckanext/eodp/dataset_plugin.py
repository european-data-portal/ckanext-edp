import os
import sys
import ckan.plugins as plugins
import translation as trs
from config import CONFIG
from ckanext.eodp.schema.helper import *
from ckanext.eodp import translation
from ckanext.eodp import maps
from ckanext.eodp import visualisation
from ckanext.eodp import license
from ckanext.eodp import mqa_plugin
from ckanext.eodp.model import setup as model_setup
from .helpers import build_language_link, \
    get_main_menu_link, \
    feedback_button, \
    unquote_url, \
    preview_available, \
    get_available_countries, \
    get_organization_image_url, \
    get_country_facet, \
    country_remove_url_param, \
    country_name_by_param, \
    get_static_facets, \
    follow_button, to_json
import ckanext.eodp.seo.helpers as seo_helpers
from model import CountryOrganization
import gettext
import pycountry
import logging
from routes.mapper import SubMapper

german = gettext.translation('iso3166', pycountry.LOCALES_DIR, languages=['de'])
german.install()

ECAS_ENABLED = CONFIG.getboolean('ecas', 'enabled')
TRANS_MOCK_ENABLED = CONFIG.getboolean('translation', 'mock_service')

log = logging.getLogger(__name__)

class DatasetPlugin(plugins.SingletonPlugin, tk.DefaultDatasetForm):

    plugins.implements(plugins.ITranslation)
    plugins.implements(plugins.IDatasetForm)
    plugins.implements(plugins.IConfigurable, inherit=True)
    plugins.implements(plugins.IPackageController, inherit=True)
    plugins.implements(plugins.IRoutes, inherit=False)
    plugins.implements(plugins.ITemplateHelpers)
    plugins.implements(plugins.IFacets, inherit=True)

    def configure(self, config):
        model_setup()


    def i18n_directory(self):
        # assume plugin is called ckanext.<myplugin>.<...>.PluginClass
        return os.path.join(os.path.dirname(__file__), 'i18n_ext')

    def i18n_locales(self):
        directory = self.i18n_directory()
        languages = [ d for
                 d in os.listdir(directory)
                 if os.path.isdir(os.path.join(directory, d))
                 ]
        return languages

    def i18n_domain(self):
        return 'ckanext-eodp'

    def get_helpers(self):
        helpers = {
            'dcat_ap_for_view': dcat_ap_package_for_view,
            'dcat_ap_for_edit': dcat_ap_package_for_edit,
            'dcat_ap_org_for_view': dcat_ap_organization_for_view,
            'dcat_ap_org_for_edit': dcat_ap_organization_for_edit,
            'dcat_ap_resource_edit': dcat_ap_resource_for_edit,
            'get_res_map_data': maps.get_res_map_data,
            'get_pkg_spatial_data': maps.get_pkg_spatial_data,
            'pkg_is_geo_enabled': maps.pkg_is_geo_enabled,
            'available_translations': trs.get_available_translations,
            'available_languages': trs.get_available_languages,
            'display_name_for_locale': trs.get_display_name_for_locale,
            'language_selection': trs.get_language_selection,
            'get_visualisation_data': visualisation.get_visualisation_data,
            'get_license_assistant_data': license.get_license_assistant_data,
            'language_link': build_language_link,
            'main_menu_link': get_main_menu_link,
            'get_config_value': CONFIG.get,
            'feedback_button': feedback_button,
            '_metadata': trs.translate,
            'is_machine_translated': trs.is_machine_translated,
            'translation_available': trs.translation_is_available,
            'unquote_url': unquote_url,
            'preview_available': preview_available,
            'available_countries': get_available_countries,
            'get_organization_image_url': get_organization_image_url,
            'get_country_facet': get_country_facet,
            'country_remove_url_param': country_remove_url_param,
            'country_name_by_param': country_name_by_param,
            'get_static_facets': get_static_facets,
            'follow_button': follow_button,
            'get_distribution_information': mqa_plugin.get_distribution_information,
            'to_json': to_json,
            'get_violation_information': mqa_plugin.get_violation_information,
            'get_mqa_result': mqa_plugin.get_mqa_result,
            'get_schemaorg_snippet': seo_helpers.get_schemaorg_snippet
        }
        return helpers

    def dataset_facets(self, facets_dict, package_type):
        facets_dict['groups'] = tk._('Categories')
        facets_dict['organization'] = tk._('Catalogues')
        facets_dict['license_id'] = tk._('Licences')
        return facets_dict

    def organization_facets(self, facets_dict, organization_type, package_type):
        facets_dict['groups'] = tk._('Categories')
        del facets_dict['organization']
        facets_dict['license_id'] = tk._('Licences')
        return facets_dict

    def group_facets(self, facets_dict, organization_type, package_type):
        facets_dict['groups'] = tk._('Categories')
        facets_dict['organization'] = tk._('Catalogs')
        facets_dict['license_id'] = tk._('Licences')
        return facets_dict

    def before_map(self, map):
        map.redirect('/', '/dataset')


        map.connect('gazetteer_autocomplete',
                    '/gazetteer/autocomplete',
                    controller='ckanext.eodp.gazetteer:GazetteerController',
                    action='autocomplete')

        map.connect('resource_visualisation',
                    '/visualisation/{id}',
                    controller='ckanext.eodp.visualisation:VisualisationController',
                    action='main')

        map.connect('/dataset/{id}.{format}',
                    controller='ckanext.eodp.sparql_store.controller:ContentTypeController',
                    action='negotiate')

        map.connect('/feeds/custom.atom',
                    controller='ckanext.eodp.feed:CustomFeedController',
                    action='custom')

        map.connect('/translation/reschedule',
                    controller='ckanext.eodp.trans.reschedule_service:TranslationService',
                    action='reschedule_trans')

        map.connect('dataset_mqa', '/dataset/mqa/{id}',
                    controller='ckanext.eodp.mqa_plugin:MQAController',
                    action='read',
                    ckan_icon='certificate')

        map.connect('dataset_similar', '/dataset/similar/{id}',
                    controller='ckanext.eodp.mqa_plugin:MQAController',
                    action='get_similar_datasets',
                    ckan_icon='tasks')

        if TRANS_MOCK_ENABLED:
            map.connect('/translation/mock',
                        controller='ckanext.eodp.trans.mock_service:MockTranslationService',
                        action='translate')

        if ECAS_ENABLED:
            map.connect('/ecas/login',
                        controller='ckanext.eodp.ecas:EcasController',
                        action='login')

            map.connect('/ecas/service',
                        controller='ckanext.eodp.ecas:EcasController',
                        action='callback')

        map.connect('user_dashboard_queries', '/dashboard/queries',
                    controller="ckanext.eodp.query_controller:QueryController",
                  action='dashboard_queries', ckan_icon='list')

        map.connect('user_queries_create', '/user/queries/{id}/new',
                    controller='ckanext.eodp.query_controller:QueryController',
                    action='new')

        map.connect('user_queries_delete', '/user/queries/{id}/delete',
                    controller='ckanext.eodp.query_controller:QueryController',
                    action='delete')

        map.connect('statistics_current_selection', '/statistics/current',
                    controller='ckanext.eodp.statistics.statistic_controller:StatisticController',
                    action='call_model_functions_current')

        map.connect('statistics_evolution_selection', '/statistics/evolution',
                    controller='ckanext.eodp.statistics.statistic_controller:StatisticController',
                    action='call_model_functions_evolution')

        map.connect('statistics_update', '/statistics/update',
                    controller='ckanext.eodp.statistics.statistic_controller:StatisticController',
                    action='update_statistics')

        return map

    def after_map(self, map):
        return map

    def _modify_package_schema(self, schema):
        schema.update(dcat_schema.dcat_ap_modify_package_schema())
        schema['resources'].update(dcat_schema.dcat_ap_modify_resource_schema())
        return schema

    def show_package_schema(self):
        schema = super(DatasetPlugin, self).show_package_schema()
        schema.update(dcat_schema.dcat_ap_show_package_schema())
        schema['resources'].update(dcat_schema.dcat_ap_show_resource_schema())
        return schema

    def create_package_schema(self):

        # let's grab the default schema in our plugin
        schema = super(DatasetPlugin, self).create_package_schema()
        return self._modify_package_schema(schema)

    def update_package_schema(self):

        schema = super(DatasetPlugin, self).update_package_schema()
        return self._modify_package_schema(schema)

    def is_fallback(self):
        # Return True to register this plugin as the default handler for
        # package types not handled by any other IDatasetForm plugin.
        return True

    def package_types(self):
        # This plugin doesn't handle any special package types, it just
        # registers itself as the default (above).
        return []

    def setup_template_variables(self, context, data_dict):
        super(DatasetPlugin, self).setup_template_variables(context, data_dict)
        #c.pkg_notes_formatted = h.render_markdown(c.pkg_dict['notes'])

    def before_search(self, search_params):
        '''
        Extensions will receive a dictionary with the query parameters,
        and should return a modified (or not) version of it.

        search_params will include an `extras` dictionary with all values
        from fields starting with `ext_`, so extensions can receive user
        input from specific fields.
        '''

        try:
            tk.request._current_obj()
            search_params['qf'] = translation.get_language_query(search_params, lang_detection=False)
        except TypeError:
            pass

        if 'sort' not in search_params:
            search_params['sort'] = u'metadata_modified desc'

        # Change the faceted search from AND to OR
        request = tk.request
        update_search = False

        request_types = [
            ('organization', 'read'),
            ('package', 'search'),
            ('group', 'read')
        ]


        try:
            urlvars = request.urlvars
            if (urlvars['controller'], urlvars['action']) in request_types:
                update_search = True
        except:
            pass

        if update_search:
            if 'before_search' in search_params['extras'] and search_params['extras']['before_search'] is False:
                del search_params['extras']['before_search']
            else:
                try:
                    filter_dict = {}
                    country_filter = set()
                    for (param, value) in request.params.items():
                        if param not in ['q', 'page', 'sort', 'country'] and len(value) and not param.startswith('_'):
                            if not param.startswith('ext_'):
                                if param in filter_dict:
                                    filter_dict[param] = filter_dict[param] + ' OR "' + value + '"'
                                else:
                                    filter_dict[param] = '"' + value + '"'
                        if param == 'country':
                            orgs = CountryOrganization.get_org_names_for_country(value.upper())
                            for org in orgs:
                                country_filter.add(org.org_name)

                    new_fq = u''
                    for key, value in filter_dict.items():
                        new_fq = new_fq + ' ' + key + ':' + '(' + value + ') '

                    if len(country_filter) > 0:
                        country_filter_string = ' OR '.join(country_filter)
                        new_fq = new_fq + ' organization:(' + country_filter_string + ')'

                    try:
                        urlvars = request.urlvars
                        if urlvars['action'] == 'read' and urlvars['controller'] == 'organization':
                            c = tk.c
                            try:
                                id = c.group_dict.get('name')
                                new_fq = new_fq + ' +organization:' + id
                            except AttributeError:#
                                pass
                            search_params['q'] = request.params.get('q', '')

                        if urlvars['action'] == 'read' and urlvars['controller'] == 'group':
                            c = tk.c
                            try:
                                id = c.group_dict.get('name')
                                new_fq = new_fq + ' +groups:' + id
                                search_params['q'] = request.params.get('q', '')
                            except AttributeError:
                                    pass

                    except (AttributeError, KeyError):
                        log.debug("No URL Vars set")

                    search_params['fq'] = new_fq + ' +dataset_type:dataset'
                except TypeError:
                    pass

        # if search_params['sort'] == u'metadata_modified desc':
        #     search_params['sort'] = u'extras_modified desc'

        search_params['facet.limit'] = 100
        return search_params

    def before_index(self, pkg_dict):
        '''
         Extensions will receive what will be given to the solr for
         indexing. This is essentially a flattened dict (except for
         multli-valued fields such as tags) of all the terms sent to
         the indexer. The extension can modify this by returning an
         altered version.
        '''

        pkg_dict = translation.index(pkg_dict)
        process_identifier_field(pkg_dict)
        return pkg_dict

    def before_view(self, data_dict):
        return data_dict

