__author__ = 'fki'

import ckan.plugins.toolkit as tk


def is_api_call(context):
    '''
    Checks if the current request is an API Call.
    This is done by simply checking if the keyword "api"
    is part of the current URL.
    ToDo Check if there is a more elegant way

    :param context:
    :return:
    '''
    request = tk.request
    if hasattr(request, 'path'):
        path = tk.request.path
        if 'api' not in path:
            return False
        else:
            return True
    elif 'api_version' in context:
        return True
    else:
        return False

    # This one does not work due to a bug in package_patch
    # if 'api_version' not in context:
    #     return False
    # else:
    #     return True


def is_post_request():
    request = tk.request
    if request.method == 'POST':
        return True
    else:
        return False


