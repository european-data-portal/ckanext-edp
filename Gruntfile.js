module.exports = function(grunt) {

  grunt.initConfig({
    css_prefix: {
    libname: {
      options: {
	    prefix: 'edp-base-'
      },
      files: {
        'ckanext/eodp/fanstatic/eu/edp-resp.css': ['ckanext/eodp/fanstatic/eu/src/edp-resp.css'],
        'ckanext/eodp/fanstatic/eu/edp-bootstrap.css': ['ckanext/eodp/fanstatic/eu/src/edp-bootstrap.css'],
        'ckanext/eodp/fanstatic/eu/edp-style.css': ['ckanext/eodp/fanstatic/eu/src/edp-style.css'],
        'ckanext/eodp/fanstatic/eu/style.css': ['ckanext/eodp/fanstatic/eu/src/style.css'],
        'ckanext/eodp/fanstatic/eu/system-base.css': ['ckanext/eodp/fanstatic/eu/src/system-base.css']
      }
    }
  }
  });

  grunt.loadNpmTasks('grunt-css-prefix');
  grunt.registerTask('buildcss', ['css_prefix']);
};
